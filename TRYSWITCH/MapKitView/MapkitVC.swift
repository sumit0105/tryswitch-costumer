//
//  MapkitVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 07/10/21.
//

import UIKit

import GoogleMaps
import GooglePlaces
import CoreLocation

class MapkitVC: UIViewController {
    
    @IBOutlet var hotspotView: UIView!
    @IBOutlet weak var btnUselo:UIButton!
    @IBOutlet weak var txtLocation :UITextField!
    
    @IBOutlet var mapViw: GMSMapView!
    

    var gmsAddress: GMSAddress?
    var locationManager:CLLocationManager!
    var zoomCamera : GMSCameraPosition?
    var currentLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hotspotView.frame = view.bounds
        self.view.addSubview(hotspotView)
        hotspotView.isHidden = true
        btnUselo.layer.cornerRadius = 20
        btnUselo.clipsToBounds = true
        
        btnUselo.layer.shadowRadius = 10
        btnUselo.layer.shadowOpacity = 1.0
//        btnUselo.backgroundColor = UIColor.purple
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
            if CLLocationManager.locationServicesEnabled(){
                locationManager.startUpdatingLocation()
            }
       // loadUI()
       // showLocations()
        
        

    }
    
    func loadUI() {
        configureGoogleMap()
    }
    
    func configureGoogleMap() {
       
        if let myLocation = currentLocation{
            setMapWith(thisCoordinate: myLocation.coordinate)
        }
        print ("myLocation")
    }
    
    func showLocations() {
        DispatchQueue.main.async { [self] in
            self.mapViw.clear()
            let locate =  Common.currentLocation()
            self.mapViw.isMyLocationEnabled = true
            self.zoomCamera = GMSCameraPosition.camera(withLatitude: locate?.coordinate.latitude ?? 0.0, longitude: locate?.coordinate.longitude ?? 0.0, zoom: 10.0)
            self.mapViw.animate(to: self.zoomCamera ?? GMSCameraPosition())

            self.mapViw.padding = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 10)
            
        }}
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D , placeName : String) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { [self] response, error in
            guard let address = response?.firstResult(), let lines = address.lines  else {
                return
            }
            let strAddress = placeName + ", " +  lines.joined(separator: "\n")
            //  self.title = strAddress
            
            
            self.gmsAddress = address
           }
    }
    
       
    func setMapWith(thisCoordinate : CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude:thisCoordinate.latitude,longitude: thisCoordinate.longitude,zoom: 17.5,bearing: 30,viewingAngle: 40)
        mapViw.camera = camera
        
    }
    
    @IBAction func btnLOCATION(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let nextvc = self.storyboard?.instantiateViewController(identifier: "PropertyVC") as! PropertyVC
            self.navigationController?.pushViewController(nextvc, animated: true)
        } else {
            let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyVC") as! PropertyVC
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        
    }
    
    @IBAction func btnback (_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnnext ( _ sender:UIButton){
        
        self.hotspotView.isHidden = false
   
}

    }
    


extension MapkitVC : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.locationManager.stopUpdatingLocation()
        setMapWith(thisCoordinate: place.coordinate)
        reverseGeocodeCoordinate(place.coordinate, placeName: place.name ?? "")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
extension MapkitVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = (placemarks ?? nil)
            if placemark?.count ?? 0 > 0{
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.name!)
                self.setMapWith(thisCoordinate: userLocation.coordinate)
                self.reverseGeocodeCoordinate(userLocation.coordinate, placeName: placemark.name ?? "")
              
            }
        }
        self.locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}

extension MapkitVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtLocation{
          //  isPickUp = true
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            self.present(autocompleteController, animated: true, completion: nil)
        }
        
    }
}


