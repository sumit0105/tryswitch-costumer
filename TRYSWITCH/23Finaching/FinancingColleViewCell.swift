//
//  FinancingColleViewCell.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 19/10/21.
//

import UIKit

class FinancingColleViewCell: UICollectionViewCell {
    
    @IBOutlet var img: UIImageView!
    
    @IBOutlet var lblName: UILabel!
    
}
