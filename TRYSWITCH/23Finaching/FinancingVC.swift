//
//  FinancingVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 19/10/21.
//

import UIKit

class FinancingVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var arrayimg: [UIImage]  = [#imageLiteral(resourceName: "icon-4 (1)"),#imageLiteral(resourceName: "33"),#imageLiteral(resourceName: "34"),#imageLiteral(resourceName: "building8"),#imageLiteral(resourceName: "36")]
    
    var arrayname = ["Banks" , "CreditCards", "Leders", "InvestmentServices", "Accountant/TaxSpecialistal"]
    
    
    

    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        
    }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayimg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FinancingColleViewCell
        cell.img.image = arrayimg[indexPath.row]
        cell.lblName.text = arrayname[indexPath.row]
        
        
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.purple.cgColor
        
        
        
        
        
        return cell
    }
    
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
        {
           return CGSize(width: 180.0, height: 130.0)
        
//        let width  = (view.frame.width-50)/3
//        return CGSize(width: width, height: width)
        
        
      
        
        }
    
    
    
    
}
