//
//  HomeVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 11/10/21.
//


import Foundation
import UIKit


extension UIImageView
{
    func makeRoundImage()
    {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    func makecornerImage(){
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
        self.layer.cornerRadius = 10.0
    }
    
    func primaryBorderColor(){
        self.layer.borderColor = UIColor(red: 127/255, green: 41/255, blue: 87/255, alpha: 1).cgColor
        self.layer.borderWidth = 1.1
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.layer.cornerRadius = 8.0
    }
    
    
}

extension UIView {
    func shadowSetFunc(){
        self.layer.cornerRadius = 2
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.40
    }
    
    
}

extension UIView {
    func getRecentPost(){
       
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
        self.layer.cornerRadius = 8.0
    }
}


extension UIButton {
    func borderset(){
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
        self.layer.cornerRadius = 15.0
    }
}
class TextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
