//
//  OutCkeckKist18BVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 08/10/21.
//

import UIKit

class OutCkeckKist18BVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    
    
    @IBOutlet var CollView: UICollectionView!
    var action:Bool = false
    var selectedIndex = Int ()
    
    var imgarray = ["Personal","Financing","Utilities","Government"]
    var lblarray = ["Personal","Financing","Utilities","Government"]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.CollView.delegate = self
        self.CollView.dataSource = self
      
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.action = false
        self.selectedIndex = Int()
        self.CollView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgarray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ColleVCellOutCkeck
        
        cell.img.image = UIImage(named: imgarray[indexPath.row])
        cell.lbl.text = lblarray[indexPath.row]
        
//        cell.layer.cornerRadius = 5
//        cell.layer.borderWidth = 1
        cell.BgView.layer.borderWidth = 1
        cell.BgView.layer.borderColor = UIColor.init(red: 64, green: 17, blue: 114).cgColor
        
        cell.BgView.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)
        
        if (action){
           
            if selectedIndex == indexPath.row
            {
               
                cell.BgView.layer.borderColor =   UIColor.init(red: 245, green: 66, blue: 8).cgColor
                cell.BgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
            }
            else
            {
                
                cell.BgView.layer.borderColor = UIColor.init(red: 64, green: 17, blue: 114).cgColor
                cell.BgView.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)
            }
        }
//        cell.layer.borderColor = UIColor.purple.cgColor
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.action = true
        selectedIndex = indexPath.row
        self.CollView.reloadData()

        switch indexPath.item {
            
        case 0 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let  nextscreen = self.storyboard?.instantiateViewController(withIdentifier: "PersonalVc")as! PersonalVc
                self.navigationController?.pushViewController(nextscreen, animated: true)
            }
           
        case 1 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                
                let  nextscreen = self.storyboard?.instantiateViewController(withIdentifier: "FinacingVc")as! FinacingVc
                self.navigationController?.pushViewController(nextscreen, animated: true)
            }
           
        case 2 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let  nextscreen = self.storyboard?.instantiateViewController(withIdentifier: "UitilitiesVC")as! UitilitiesVC
                self.navigationController?.pushViewController(nextscreen, animated: true)
            }
           
        case 3 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let  nextscreen = self.storyboard?.instantiateViewController(withIdentifier: "GovernmentVc")as! GovernmentVc
                self.navigationController?.pushViewController(nextscreen, animated: true)
            }
           
        default:
            
            let  nextscreen = self.storyboard?.instantiateViewController(withIdentifier: "GovernmentVC")as! GovernmentVC
            self.navigationController?.pushViewController(nextscreen, animated: true)
        }
        
       
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width)/2 - 10
        return CGSize(width: size, height: (size))

     
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        20
    }
    
    
    
    @IBAction func btnback (_sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    

}
