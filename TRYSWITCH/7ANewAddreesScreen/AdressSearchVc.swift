//
//  AdressSearchVcViewController.swift
//  TRYSWITCH
//
//  Created by Anubhav on 28/10/21.
//

import UIKit
import SideMenuSwift
class AdressSearchVc: UIViewController {
    
    @IBOutlet weak var bgView: UIView!
        
   
    @IBOutlet var stringView1: [UIView]!
    @IBOutlet weak var tablView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tablView.delegate = self
        tablView.delegate = self
        for item in stringView1{
            item.setBorderColor(borderColor: UIColor(red: 73, green: 0, blue: 119), cornerRadiusBound: 20)
        }
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5
        )
    }
    @IBAction func btn_Next(_ sendar:UIButton){
        let StoryB = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil)
        let vc = StoryB.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func BackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }

}
extension AdressSearchVc:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            switch indexPath.section {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "NearByTableCell") as! NearByTableCell
                cell.selectionStyle = .none
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PopularLacTableCell") as! PopularLacTableCell
                cell.selectionStyle = .none
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesUmayTableCell") as! PlacesUmayTableCell
                cell.selectionStyle = .none
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesUmayTableCell") as! PlacesUmayTableCell
                cell.selectionStyle = .none
                return cell
            }
            
            
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0 : return UITableView.automaticDimension
        case 1: return UITableView.automaticDimension
        case 2: return 700
            
        default:
            return 500
        }
    }
    
}
