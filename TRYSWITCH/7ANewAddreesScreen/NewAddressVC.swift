//
//  NewAddressVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 13/10/21.
//

import UIKit
import SideMenuSwift
class NewAddressVC: UIViewController {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var searchTf: UITextField!
    @IBOutlet weak var tablView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tablView.delegate = self
        tablView.delegate = self
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5
        )
        
    }
    @IBAction func btn_Next(_ sendar:UIButton){
        let StoryB = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil)
        let vc = StoryB.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btn_SearchTf(_ sendar:UIButton){
        let vc = storyboard?.instantiateViewController(withIdentifier: "AdressSearchVc") as! AdressSearchVc
        self.navigationController?.pushViewController(vc, animated: false)
    }

    @IBAction func BackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension NewAddressVC:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            switch indexPath.section {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "NearByTableCell") as! NearByTableCell
                cell.selectionStyle = .none
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PopularLacTableCell") as! PopularLacTableCell
                cell.selectionStyle = .none
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesUmayTableCell") as! PlacesUmayTableCell
                cell.selectionStyle = .none
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesUmayTableCell") as! PlacesUmayTableCell
                cell.selectionStyle = .none
                return cell
            }
            
            
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0 : return UITableView.automaticDimension
        case 1: return UITableView.automaticDimension
        case 2: return 700
            
        default:
            return 500
        }
    }
    
}
