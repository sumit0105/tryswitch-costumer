//
//  8MServProvidVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 13/10/21.
//

import UIKit

class _MServProvidVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var txtSearchTf: TextField!
    
    @IBOutlet var TblView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearchTf.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)
        self.TblView.delegate = self
        self.TblView.dataSource = self
        
    }
    
    @IBAction func Back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated:true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! _MTblViewCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        148
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = serviceProviderVc.instantiate(fromAppStoryboard: .Service)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
