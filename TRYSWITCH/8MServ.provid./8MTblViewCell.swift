//
//  8MTblViewCell.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 13/10/21.
//

import UIKit

class _MTblViewCell: UITableViewCell {
    @IBOutlet weak var Bgview: UIView!
    
    override func awakeFromNib(){
        super.awakeFromNib()
        Bgview.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
       
    }
}
