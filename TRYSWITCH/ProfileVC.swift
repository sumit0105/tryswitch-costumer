//
//  ViewController.swift
//  design
//
//  Created by satyam mac on 27/10/21.
//

import UIKit

class ProfileVc: UIViewController {
    
    @IBOutlet var bgView: [UIView]!
    override func viewDidLoad() {
        super.viewDidLoad()
   
        for item in bgView {
            
            item.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
        
        }
    }
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func changePassBtn(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVc") as! ChangePasswordVc
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func editProfileBtn(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileVc") as! EditProfileVc
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


//extension UIView {
//    
//    func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat) {
//        
//        self.layer.shadowColor = shadowColor.cgColor
//        self.layer.shadowOffset = offSet
//        self.layer.shadowOpacity = opacity
//        self.layer.shadowRadius = shadowRadius
//    }
//}
