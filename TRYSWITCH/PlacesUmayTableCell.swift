//
//  PlacesUmayTableCell.swift
//  TRYSWITCH
//
//  Created by Anubhav on 28/10/21.
//

import UIKit

class PlacesUmayTableCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var iCollectionView:UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.iCollectionView.delegate = self
        self.iCollectionView.dataSource = self
        iCollectionView.register(UINib(nibName: "PopularLoalitiesCollCell", bundle: nil), forCellWithReuseIdentifier: "PopularLoalitiesCollCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularLoalitiesCollCell", for: indexPath) as! PopularLoalitiesCollCell
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width)/2
        return CGSize(width: size, height: (size ))



    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0   }
}

