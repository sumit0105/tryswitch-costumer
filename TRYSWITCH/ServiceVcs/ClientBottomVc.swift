//
//  ClientBottomVc.swift
//  Realtor
//
//  Created by Anubhav on 02/11/21.
//

import UIKit

class ClientBottomVc:UIViewController, Demoable {
    static var name: String = {"hgcycytw"}()
    static let identifier = "ClientBottomVc"
    
//    @IBOutlet weak var disCribeTV: UITextView!
//    @IBOutlet weak var TbleVw:UITableView!
    @IBOutlet var btnround: [UIButton]!
    @IBOutlet var border_Tf: [UITextField]!
    override func viewDidLoad() {
        super.viewDidLoad()
//        TbleVw.delegate = self
//        TbleVw.dataSource = self
//
        for tf in border_Tf{
            tf.setBorderColor(borderColor: .gray, cornerRadiusBound: 20)
            
        }
        for tf in btnround{
            tf.setBorderColor(borderColor: UIColor(red: 245, green: 66, blue: 8), cornerRadiusBound: 20)
            
        }
        let imageview = UIImageView(image: UIImage(named: "18"))
        imageview.contentMode = .center
        let rightPadding: CGFloat = 0 //--- change right padding
        imageview.frame = CGRect(x: 0, y: 0, width: imageview.frame.size.width + rightPadding , height:imageview.frame.size.height)
//
        
        // set the rightView UIView as the textField's rightView
//        disCribeTV.setBorderColor(borderColor: .gray, cornerRadiusBound: 20)
       
        
    }
    override func viewDidLayoutSubviews() {
//        setScrollIndicatorColor()
    }
    static func openDemo(from parent: UIViewController, in view: UIView?) {
        let useInlineMode = view != nil
        
        let controller = UIStoryboard(name: "SignUp", bundle: nil).instantiateViewController(withIdentifier: "ClientBottomVc") as! ClientBottomVc
         
        let options = SheetOptions(
            useFullScreenMode: false,
            useInlineMode: useInlineMode)
        let sheet = SheetViewController(controller: controller, sizes: [.percent(0.6)], options: options)
//        sheet.hasBlurBackground = true
        sheet.overlayView.isOpaque = true
        sheet.gripSize = CGSize(width: 150, height: 8)
        sheet.minimumSpaceAbovePullBar = 100
        sheet.pullBarBackgroundColor = .white
//        sheet.dismissOnPull = false
        sheet.dismissOnOverlayTap = false
        addSheetEventLogging(to: sheet)
        
        if let view = view {
            sheet.animateIn(to: view, in: parent)
        } else {
            parent.present(sheet, animated: true, completion: nil)
        }
    }
//    func setScrollIndicatorColor() {
//            for view in self.TbleVw.subviews {
//                if view.isKind(of: UIImageView.self),
//                    let imageView = view as? UIImageView,
//                   let image = imageView.image  {
//
//                    imageView.tintColor = UIColor.init(red: 245, green: 66, blue: 8)
//                   imageView.image = image.withRenderingMode(.alwaysTemplate)
//               }
//            }
//
//            self.TbleVw.flashScrollIndicators()
//        }

}
extension ClientBottomVc:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientBottomCell") as! ClientBottomCell
//        setScrollIndicatorColor()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        46
    }
    
    
}
