//
//  contactLanderVc.swift
//  Customer Service
//
//  Created by satyam mac on 23/11/21.
//

import UIKit

class contactLanderVc: UIViewController {

    @IBOutlet weak var borderView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        borderView.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
