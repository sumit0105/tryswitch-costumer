//
//  estateJournyCell.swift
//  Customer Service
//
//  Created by satyam mac on 23/11/21.
//

import UIKit

class estateJournyCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setBorderColor(borderColor: UIColor.init(red: 64, green: 17, blue: 114), cornerRadiusBound: 20)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
