//
//  locationFilterCell.swift
//  Customer Service
//
//  Created by satyam mac on 29/11/21.
//

import UIKit

class locationFilterCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet var stringView: [UIView]!
    override func awakeFromNib() {
        super.awakeFromNib()
        for item in stringView{
            item.setBorderColor(borderColor: UIColor.init(red: 63, green: 17, blue: 114), cornerRadiusBound: 20)
        }
        bgView.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}



