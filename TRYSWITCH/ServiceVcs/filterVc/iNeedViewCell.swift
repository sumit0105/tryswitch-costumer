//
//  iNeedViewCell.swift
//  Customer Service
//
//  Created by satyam mac on 29/11/21.
//

import UIKit

class iNeedViewCell: UITableViewCell {
    @IBOutlet var lbl: [UILabel]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        for i in lbl{
            i.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 5)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
