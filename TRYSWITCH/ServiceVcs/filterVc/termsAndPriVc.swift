//
//  termsAndPriVc.swift
//  Customer Service
//
//  Created by satyam mac on 30/11/21.
//

import UIKit

class termsAndPriVc: UIViewController {

    @IBOutlet weak var tableV: UITableView!
    var headingDs = ["Access to your Personal Informatin","How Long Do We Return User Data","Log Information","Changes To This Statement"]

    override func viewDidLoad() {
       
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "sourcesViewCell", bundle: nil), forCellReuseIdentifier: "sourcesViewCell")
    }
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension termsAndPriVc:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headingDs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
            let cell = tableView.dequeueReusableCell(withIdentifier: "sourcesViewCell") as! sourcesViewCell
        cell.hedingLbl.text = headingDs[indexPath.row]
            return cell
           
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        192
    }
    
       
    }

