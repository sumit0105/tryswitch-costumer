//
//  filterVc.swift
//  Customer Service
//
//  Created by satyam mac on 29/11/21.
//

import UIKit

class filterVc: UIViewController,Demoable {

   
    static var name: String = {"hgcycytw"}()
    static let identifier = "filterVc"
    
   

    @IBOutlet weak var tableV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
      //  view.translatesAutoresizingMaskIntoConstraints = false
        
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "locationFilterCell", bundle: nil), forCellReuseIdentifier: "locationFilterCell")
        tableV.register(UINib(nibName: "iPreferCell", bundle: nil), forCellReuseIdentifier: "iPreferCell")
        tableV.register(UINib(nibName: "propertyTypeCell", bundle: nil), forCellReuseIdentifier: "propertyTypeCell")
        tableV.register(UINib(nibName: "iNeedViewCell", bundle: nil), forCellReuseIdentifier: "iNeedViewCell")
        tableV.register(UINib(nibName: "PriceRangeCell1", bundle: nil), forCellReuseIdentifier: "PriceRangeCell1")
        
    }
        static func openDemo(from parent: UIViewController, in view: UIView?) {
        let useInlineMode = view != nil

        let controller = UIStoryboard(name: "Service", bundle: nil).instantiateViewController(withIdentifier: "filterVc") as! filterVc

        let options = SheetOptions(
            useFullScreenMode: false,
            useInlineMode: useInlineMode)
        let sheet = SheetViewController(controller: controller, sizes: [.percent(0.6),.fullscreen], options: options)
//        sheet.hasBlurBackground = true
        sheet.overlayView.isOpaque = true
        sheet.gripSize = CGSize(width: 150, height: 8)
        sheet.minimumSpaceAbovePullBar = 100
        sheet.pullBarBackgroundColor = .white
        //sheet.dismissOnPull = false
        sheet.dismissOnOverlayTap = false
        addSheetEventLogging(to: sheet)

        if let view = view {
            sheet.animateIn(to: view, in: parent)
        } else {
            parent.present(sheet, animated: true, completion: nil)
        }
    }
    
}
    extension filterVc:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
            if indexPath.row == 0{
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "locationFilterCell") as! locationFilterCell
               return cell1
            }
                else if indexPath.row == 1{
                let cell2 = tableView.dequeueReusableCell(withIdentifier: "iPreferCell") as! iPreferCell
                    return cell2
                }
                else if indexPath.row == 2{
                let cell3 = tableView.dequeueReusableCell(withIdentifier: "propertyTypeCell") as! propertyTypeCell
                    return cell3
                }
                else if indexPath.row == 3{
                let cell4 = tableView.dequeueReusableCell(withIdentifier: "iNeedViewCell") as! iNeedViewCell
                    return cell4
                }
                else if indexPath.row == 4  {
                let cell5 = tableView.dequeueReusableCell(withIdentifier: "PriceRangeCell1") as! PriceRangeCell1
                    return cell5
                }
            return UITableViewCell()
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             
           return UITableView.automaticDimension
            
        }
    }
