//
//  trySwitchListVc.swift
//  Customer Service
//
//  Created by satyam mac on 26/11/21.
//

import UIKit

class trySwitchListVc: UIViewController {

    @IBOutlet var bordersView: [UIView]!
    @IBOutlet weak var imgAll: UIImageView!
    @IBOutlet weak var allLbl: UILabel!
    @IBOutlet weak var inCopletedLbl: UILabel!
    @IBOutlet weak var imgIncompleted: UIImageView!
    @IBOutlet weak var completedLbl: UILabel!
    @IBOutlet weak var imgCompleted: UIImageView!
     
    @IBOutlet weak var tableVi:UITableView!
    var rowCount:Int = 5
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        
            tableVi.dataSource = self
            tableVi.delegate = self
            tableVi.register(UINib(nibName: "trySwitchViewCell1", bundle: nil), forCellReuseIdentifier: "trySwitchViewCell1")
        
        for bg in bordersView{
            if bg.tag == 1{ bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)}else{ bg.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)}
           
        
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func FilterBtn(_ sender: UIButton) {
        let vc = collaboratorsVc.instantiate(fromAppStoryboard: .Service)
        self.present(vc, animated: true, completion: nil)
    }
 
    @IBAction func filterBtns(_ sender: UIButton) {
        if sender.tag == 1{
            allLbl.textColor = UIColor.init(red: 68, green: 0, blue: 99)
            imgAll.image = UIImage(named: "4-1")
            imgCompleted.image = UIImage(named: "plaincircle")
            imgIncompleted.image = UIImage(named: "plaincircle")
            
            completedLbl.textColor = .lightGray
            inCopletedLbl.textColor = .lightGray
            for bg in bordersView{
                if bg.tag == 1{ bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)}else{ bg.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)}
               
            }
            self.rowCount = 5
            self.tableVi.reloadData()
            
        }else if sender.tag == 2 {
            imgAll.image = UIImage(named: "plaincircle")
            imgCompleted.image = UIImage(named: "4-1")
            imgIncompleted.image = UIImage(named: "plaincircle")
            completedLbl.textColor = UIColor.init(red: 68, green: 0, blue: 99)
            allLbl.textColor = .lightGray
            inCopletedLbl.textColor = .lightGray
            for bg in bordersView{
                if bg.tag == 2{ bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)}else{ bg.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)}
               
            }
            self.rowCount = 3
            self.tableVi.reloadData()
        }else if sender.tag == 3{
            imgAll.image = UIImage(named: "plaincircle")
            imgCompleted.image = UIImage(named: "plaincircle")
            imgIncompleted.image = UIImage(named: "4-1")
            allLbl.textColor = .lightGray
            completedLbl.textColor = .lightGray
            inCopletedLbl.textColor = UIColor.init(red: 68, green: 0, blue: 99)
            for bg in bordersView{
                if bg.tag == 3{ bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)}else{ bg.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)}
               
            }
            self.rowCount = 2
            self.tableVi.reloadData()
        }
        
    }
    
    
    
    @IBAction func addListingBtn(_ sender: Any) {
       
    }
}
 
    extension trySwitchListVc:UITableViewDelegate, UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            rowCount
        }
        
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
    //cell.selectionStyle = .none
        let cell =  tableView.dequeueReusableCell(withIdentifier: "trySwitchViewCell1") as! trySwitchViewCell1
        
        return cell
      
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        110
        
    }
        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            120
        }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TasksVC.instantiate(fromAppStoryboard: .Main)
        vc.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
