//
//  serviceInformationCell.swift
//  Customer Service
//
//  Created by satyam mac on 30/11/21.
//

import UIKit

class serviceInformationCell: UITableViewCell {
    

    @IBOutlet weak var searchTfBgV: UIView!
    @IBOutlet var btnMessage: UIButton!
    
    @IBOutlet var btnCall: UIButton!
    @IBOutlet var borderV: [UIView]!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnMessage.roundCorners(radius: 20)
        self.btnCall.setBorderColor(borderColor: .red, cornerRadiusBound: 20)
        
        
        
        
        
        searchTfBgV.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
        for i in borderV{
            i.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 18)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
