//
//  serviceProviderVc.swift
//  Customer Service
//
//  Created by satyam mac on 30/11/21.
//

import UIKit

class serviceProviderVc: UIViewController {
    @IBOutlet weak var tableV: UITableView!
    var arrIndexPaths:[IndexPath] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "reviewCusCell", bundle: nil), forCellReuseIdentifier: "reviewCusCell")
        tableV.register(UINib(nibName: "serviceInformationCell", bundle: nil), forCellReuseIdentifier: "serviceInformationCell")
        tableV.register(UINib(nibName: "serviceSVc", bundle: nil), forCellReuseIdentifier: "serviceSVc")
      

        
    }

    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension serviceProviderVc:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 1 {
        return 4
    }else{
        return 1
    }
    
}
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch indexPath.section {
        case 0 :
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "serviceInformationCell") as! serviceInformationCell
            cell1.selectionStyle = .none
            return cell1
        case 1 :
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "serviceSVc") as! serviceSVc
            
//            cell1.hieghtBottom.constant = 50
            cell1.selectionStyle = .none
            
            cell1.btnOpenClose.tag = indexPath.row
            
            cell1.btnOpenClose.addTarget(self,action: #selector(self.hideSection(sender:)),                               for: .touchUpInside)
            
            if arrIndexPaths.contains(indexPath) {
           
            }
            cell1.layoutSubviews()
            return cell1
            
        case 2 :
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "reviewCusCell") as! reviewCusCell
            cell1.selectionStyle = .none
            return cell1
        default:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "reviewCusCell") as! reviewCusCell
            cell1.selectionStyle = .none
            return cell1
        }
     
      
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
            if let cell1 = tableView.cellForRow(at: indexPath) as? serviceSVc {
               
                UIView.animate(withDuration: 0.3) {
                    cell1.bottomView.isHidden = !cell1.bottomView.isHidden
                }
                if cell1.bottomView.isHidden {
                    cell1.btnOpenClose.setTitle("+", for: .normal)
                    
    //                        cell.hieghtBottom.constant = 50
                }else{
                    
                    cell1.btnOpenClose.setTitle("-", for: .normal)
    //                        cell.hieghtBottom.constant = 600
                }
                DispatchQueue.main.async {
                    tableView.beginUpdates()
                    tableView.endUpdates()
        //                    tableView.deselectRow(at: indexPath, animated: true)
                }
            }
                
        
                
        }
    
    @objc func hideSection(sender: UIButton) {
        
        let selectedIndexPath = NSIndexPath.init(row: sender.tag, section: 1)
        arrIndexPaths.append(selectedIndexPath as IndexPath)
       
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
           return UITableView.automaticDimension
      
}
}
