//
//  ContactVc.swift
//  TRYSWITCH
//
//  Created by Anubhav on 01/12/21.
//

import UIKit
import ContactsUI
class ContactVc: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
    
    var contacts = ["Jhon","Methew","Hall","Sheron","Julie","Ana","Flora"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
//           let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)]
//           let request = CNContactFetchRequest(keysToFetch: keys)
//
//           let contactStore = CNContactStore()
//           do {
//               try contactStore.enumerateContacts(with: request) {
//                   (contact, stop) in
//                   // Array containing all unified contacts from everywhere
//                   self.contacts.append(contact)
//               }
//           }
//           catch {
//               print("unable to fetch contacts")
//           }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        cell.contactLbl.text = contacts[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
