//
//  trySwitchViewCell1.swift
//  Customer Service
//
//  Created by satyam mac on 30/11/21.
//

import UIKit

class trySwitchViewCell1: UITableViewCell {

    @IBOutlet weak var Bgview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Bgview.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
