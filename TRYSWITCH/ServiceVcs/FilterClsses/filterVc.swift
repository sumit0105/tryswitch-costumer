//
//  filterVc.swift
//  Customer Service
//
//  Created by satyam mac on 29/11/21.
//

import UIKit

class filterVc: UIViewController {

    @IBOutlet weak var tableV: UITableVie!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "locationFilterCell", bundle: nil), forCellReuseIdentifier: "locationFilterCell")
        tableV.register(UINib(nibName: "iPreferCell", bundle: nil), forCellReuseIdentifier: "iPreferCell")
        tableV.register(UINib(nibName: "propertyTypeCell", bundle: nil), forCellReuseIdentifier: "propertyTypeCell")
        tableV.register(UINib(nibName: "iNeedViewCell", bundle: nil), forCellReuseIdentifier: "iNeedViewCell")
        tableV.register(UINib(nibName: "PriceRangeCell1", bundle: nil), forCellReuseIdentifier: "PriceRangeCell1")

        
    }
}
    extension filterVc:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
            if indexPath.row == 0{
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "locationFilterCell") as! locationFilterCell
               return cell1
            }
                else if indexPath.row == 1{
                let cell2 = tableView.dequeueReusableCell(withIdentifier: "iPreferCell") as! iPreferCell
                    return cell2
                }
                else if indexPath.row == 2{
                let cell3 = tableView.dequeueReusableCell(withIdentifier: "propertyTypeCell") as! propertyTypeCell
                    return cell3
                }
                else if indexPath.row == 3{
                let cell4 = tableView.dequeueReusableCell(withIdentifier: "iNeedViewCell") as! iNeedViewCell
                    return cell4
                }
                else if indexPath.row == 4  {
                let cell5 = tableView.dequeueReusableCell(withIdentifier: "PriceRangeCell1") as! PriceRangeCell1
                    return cell5
                }
            return UITableViewCell()
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             
           return UITableView.automaticDimension
            
        }
    }
