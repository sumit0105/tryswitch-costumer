//
//  collaboratorsVc.swift
//  Customer Service
//
//  Created by satyam mac on 23/11/21.
//

import UIKit

class collaboratorsVc: UIViewController{

    @IBOutlet weak var tabBgView: UIStackView!
    @IBOutlet weak var tableV: UITableView!
    @IBOutlet weak var hotSpotView: UIView!
    @IBOutlet weak var fVieew: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.addObserver(forName: NSNotification.Name("Alert_Dismiss"), object: nil, queue: nil) { (Noti) in
//            let status = Noti.object as? Int ?? 101
//            print(status)
//            if status == 0 {
//                self.dismiss(animated: true, completion: nil)
//            }
//
//        }
        
        hotSpotView.frame = self.view.bounds
        self.view.addSubview(hotSpotView)
        hotSpotView.isHidden = true
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "collaboratorsCell", bundle: nil), forCellReuseIdentifier: "collaboratorsCell")
        tabBgView.setBorderColor(borderColor: UIColor.init(red: 245, green: 66, blue: 8), cornerRadiusBound: 20
        )
        //fVieew.roundCornersTopleft_Bottomleft(radius: 20)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismiss2(_ sender: Any) {
        self.hotSpotView.isHidden = false
    }
}
    extension collaboratorsVc:UITableViewDelegate,UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 5
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
                let cell = tableView.dequeueReusableCell(withIdentifier: "collaboratorsCell") as! collaboratorsCell
            cell.btnC = {
                self.hotSpotView.isHidden = false
            }
                return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return 65
        }


}
