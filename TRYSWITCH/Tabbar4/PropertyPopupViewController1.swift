//
//  PropertyPopupViewController1.swift
//  TRYSWITCH
//
//  Created by Anubhav on 06/12/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import FittedSheets
class PropertyPopupViewController1:UIViewController, Demoable {
    
    static func openDemo(from parent: UIViewController, in view: UIView?) {
        
    }
    
    
    static var name: String = {"hgcycyt"}()
    static var imageData:[UIImage]? = []
    static var dict :[String:String]!
    static let identifier = "PropertyPopupViewController"
    @IBOutlet var tblView:UITableView!
    @IBOutlet weak var trySwitchPopUp: UIView!
   var delegate: PopUpProtocolAction?
    var btnAction:()->() = {}
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
//        trySwitchPopUp.frame = view.bounds
//        view.addSubview(trySwitchPopUp)
//        trySwitchPopUp.isHidden = true
        tblView.delegate = self
        tblView.dataSource = self
    
    }
    
    @IBAction func publishListBtn(_ sender: Any) {
        btnAction()
    }


    override func viewWillAppear(_ animated: Bool) {
//        self.dialogBoxView.popIn()
    }
  
    static func showPopup(parentVC: UIViewController){
        
        //creating a reference for the dialogView controller
        if let popupViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "") as? PropertyPopupViewController {
            popupViewController.modalPresentationStyle = .custom
            popupViewController.modalTransitionStyle = .crossDissolve
            
            //presenting the pop up viewController from the parent viewController

            parentVC.present(popupViewController, animated: true)
        }
    }
}
extension PropertyPopupViewController1:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "propertyCell") as! propertyCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 690
    }
    
    
   
}
