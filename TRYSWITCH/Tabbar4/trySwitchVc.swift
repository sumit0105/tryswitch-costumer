//
//  trySwitchVc.swift
//  TRYSWITCH
//
//  Created by Anubhav on 29/10/21.
//

import UIKit

class trySwitchVc: UIViewController {
    @IBOutlet weak var itableView:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        itableView.register(UINib(nibName: "AddToMyWishlistVc", bundle: nil), forCellReuseIdentifier: "AddToMyWishlistVc")
    }
    
}
extension trySwitchVc:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "AddToMyWishlistVc") as! AddToMyWishlistVc
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let vc = trySwitchListVc.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
