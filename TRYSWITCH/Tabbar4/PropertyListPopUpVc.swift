//
//  PropertyListPopUpVc.swift
//  TRYSWITCH
//
//  Created by Anubhav on 07/12/21.
//

import UIKit

class PropertyListPopUpVc: UIViewController {

    @IBOutlet var hotspotView1: UIView!
    @IBOutlet var hotspotView2: UIView!
    @IBOutlet weak var bg2View: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bg2View.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 21)
        hotspotView1.frame = view.bounds
        self.view.addSubview(hotspotView1)
        hotspotView1.isHidden = false
        hotspotView2.frame = view.bounds
        self.view.addSubview(hotspotView2)
        hotspotView2.isHidden = true
        
          }
    

    @IBAction func dismisBtn(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func BtnView1(_ sender: Any) {
        self.hotspotView1.isHidden = true
        self.hotspotView2.isHidden = false
        
    }
    
    
    
    
   
}
