//
//  taskCustomerVc.swift
//  Customer Service
//
//  Created by satyam mac on 06/12/21.
//

import UIKit

class taskCustomerVc: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var bordersView: [UIView]!
     
    @IBOutlet weak var imgAll: UIImageView!
    @IBOutlet weak var allLbl: UILabel!
    @IBOutlet weak var inCopletedLbl: UILabel!
    @IBOutlet weak var imgIncompleted: UIImageView!
    @IBOutlet weak var completedLbl: UILabel!
    @IBOutlet weak var imgCompleted: UIImageView!
     
    @IBOutlet weak var tableView: UITableView!
    var rowCount:Int = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.tabBarController?.tabBar.isHidden = true
        for bg in bordersView{
            if bg.tag == 1{ bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)}else{ bg.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)}
           
        }
    }
    
    @IBAction func filterBtns(_ sender: UIButton) {
        if sender.tag == 1{
            allLbl.textColor = UIColor.init(red: 68, green: 0, blue: 99)
            imgAll.image = UIImage(named: "4-1")
            imgCompleted.image = UIImage(named: "plaincircle")
            imgIncompleted.image = UIImage(named: "plaincircle")
            
            completedLbl.textColor = .lightGray
            inCopletedLbl.textColor = .lightGray
            for bg in bordersView{
                if bg.tag == 1{ bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)}else{ bg.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)}
               
            }
            self.rowCount = 5
            self.tableView.reloadData()
            
        }else if sender.tag == 2 {
            imgAll.image = UIImage(named: "plaincircle")
            imgCompleted.image = UIImage(named: "4-1")
            imgIncompleted.image = UIImage(named: "plaincircle")
            completedLbl.textColor = UIColor.init(red: 68, green: 0, blue: 99)
            allLbl.textColor = .lightGray
            inCopletedLbl.textColor = .lightGray
            for bg in bordersView{
                if bg.tag == 2{ bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)}else{ bg.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)}
               
            }
            self.rowCount = 10
            self.tableView.reloadData()
        }else if sender.tag == 3{
            imgAll.image = UIImage(named: "plaincircle")
            imgCompleted.image = UIImage(named: "plaincircle")
            imgIncompleted.image = UIImage(named: "4-1")
            allLbl.textColor = .lightGray
            completedLbl.textColor = .lightGray
            inCopletedLbl.textColor = UIColor.init(red: 68, green: 0, blue: 99)
            for bg in bordersView{
                if bg.tag == 3{ bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)}else{ bg.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)}
               
            }
            self.rowCount = 2
            self.tableView.reloadData()
        }
        
    }
    
    
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addTask(_ sender: Any) {
        let vc = createTaskVc.instantiate(fromAppStoryboard: .SignUp)
        vc.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell =  tableView.dequeueReusableCell(withIdentifier: "CheckListCell") as! CheckListCell
 
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        50
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        60
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = NotificationVc.instantiate(fromAppStoryboard: .SignUp)
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//

}
//class tryMyTaskCell:UITableViewCell{
//
//}
