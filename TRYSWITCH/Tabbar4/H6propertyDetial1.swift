//
//  H6propertyDetial1.swift
//  TRYSWITCH
//
//  Created by Anubhav on 06/12/21.
//

import UIKit
import FittedSheets

class H6propertyDetial1: SimpleDemo {
     
    @IBOutlet weak var trySwitchPopUp: UIView!
   
    @IBOutlet weak var sucessPopUp: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        trySwitchPopUp.frame = view.bounds
        view.addSubview(trySwitchPopUp)
        trySwitchPopUp.isHidden = true
     
        sucessPopUp.frame = view.bounds
        view.addSubview(sucessPopUp)
        sucessPopUp.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
            let useInlineMode = view != nil
            
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PropertyPopupViewController1") as! PropertyPopupViewController1
            controller.btnAction =  {
                let vc = PropertyListPopUpVc.instantiate(fromAppStoryboard: .Main)
                self.navigationController?.pushViewController(vc, animated: false)
               
            }
        let sheet = SheetViewController(
            controller: controller,
            sizes: [.percent(0.6),.percent(0.9) ],
            options: SheetOptions(useInlineMode: useInlineMode))
        sheet.allowPullingPastMaxHeight = false
        sheet.allowPullingPastMinHeight = false
        
        sheet.dismissOnPull = false
        sheet.dismissOnOverlayTap = false
        sheet.overlayColor = UIColor.clear
        
        sheet.contentViewController.view.layer.shadowColor = UIColor.black.cgColor
        sheet.contentViewController.view.layer.shadowOpacity = 0.1
        sheet.contentViewController.view.layer.shadowRadius = 10
        sheet.allowGestureThroughOverlay = true
        
        H6propertyDetial1.addSheetEventLogging(to: sheet)
        
        if let view = view {
            sheet.animateIn(to: view, in: self)
        } else {
            self.present(sheet, animated: true, completion: nil)
        }
    }
        
    @IBAction func sucessPopupBtn(_ sender: UIButton) {
        self.sucessPopUp.isHidden = true
    }
    @IBAction func trySwitchPopupBtn(_ sender: UIButton) {
        self.sucessPopUp.isHidden = false
        self.trySwitchPopUp.isHidden = true
        
       
    }
    
    
   
    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
