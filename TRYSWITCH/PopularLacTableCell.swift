//
//  PopularLacTableCell.swift
//  TRYSWITCH
//
//  Created by Anubhav on 28/10/21.
//

import UIKit

class PopularLacTableCell:UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var iCollectionView:UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.iCollectionView.delegate = self
        self.iCollectionView.dataSource = self
        iCollectionView.register(UINib(nibName: "PlacesUmayLikeCell", bundle: nil), forCellWithReuseIdentifier: "PlacesUmayLikeCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlacesUmayLikeCell", for: indexPath) as! PlacesUmayLikeCell
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let size = CGSize(width: 160, height: 160)
        return size
    }
}
