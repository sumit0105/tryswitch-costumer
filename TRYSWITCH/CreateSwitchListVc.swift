//
//  CreateSwitchListVc.swift
//  design
//
//  Created by satyam mac on 28/10/21.
//

import UIKit

class CreateSwitchListVc: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func CreateDismissBtn(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    @IBAction func btnCreatNew(_ sender: Any) {
       
        let storyB = UIStoryboard(name: "PopUp", bundle: nil)
        let vc: PopupListingSuccessVc = storyB.instantiateViewController(withIdentifier: "PopupListingSuccessVc") as! PopupListingSuccessVc
        
        self.addChild(vc)
        
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
