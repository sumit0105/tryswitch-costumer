//
//  DoYouVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 30/09/21.
//

import UIKit

class DoYouVC: UIViewController {

    @IBOutlet var shadowView: [UIView]!
//    @IBOutlet weak var btnAtt: UIButton!
    @IBOutlet var v3: [UIView]!
    
    @IBOutlet weak var v5: UIView!
    @IBOutlet weak var v4: UIView!
    @IBOutlet weak var v1: UIView!
    @IBOutlet weak var v2: UIView!
//    @IBOutlet weak var btn2: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
//        btnAtt.addShadow()
//        btn2.addShadow()
        for i in v3 {
            i.roundCorners(corners: [.topLeft], radius: 10)
        }
        v4.roundCorners(corners: [.bottomRight], radius: 10)
        v5.roundCorners(corners: [.bottomRight], radius: 10)
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(tapEventDetected1(gesture:)))
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(tapEventDetected2(gesture:)))
        for i in shadowView {
            i.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
          
//            img.topRoundCorner(radius: 10, color: .white, borderWidth: 0)
//            v1.roundCorners(corners: [.topLeft], radius: 20)
        }
        
        v1.addGestureRecognizer(gesture1)
        v2.addGestureRecognizer(gesture2)
    }
    
    @objc func tapEventDetected1(gesture:UITapGestureRecognizer){

        if gesture.state == .recognized{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                self.v1.backgroundColor = UIColor.init(red: 237, green: 245, blue: 255)
            }
               
                
            }

        if gesture.state == .ended{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
             
                let FiveVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController5a") as! ViewController5a
                self.navigationController?.pushViewController(FiveVC, animated: true)
                
                self.v1.backgroundColor = UIColor.init(red: 255, green: 255, blue: 255)
                
            }
           
            
            }
        
        
    }
    @objc func tapEventDetected2(gesture:UITapGestureRecognizer){

        if gesture.state == .recognized{
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                    self.v2.backgroundColor = UIColor.init(red: 237, green: 245, blue: 255)
                }
                
            }

        if gesture.state == .ended{
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
               
                let FiveVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController5a") as! ViewController5a
                self.navigationController?.pushViewController(FiveVC, animated: true)
                self.v2.backgroundColor = UIColor.init(red: 255, green: 255, blue: 255)
            }
            
           
            }
        
    }
    
    @IBAction func btnOwnClick(_ sender: UIButton) {
        sender.addShadow()
        
      
        
    }
    
    
    
}
