//
//  Common.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 11/10/21.
//

import Foundation
import UIKit
import SystemConfiguration
import CoreLocation
import EventKit

final class Common {
    static var locManager = CLLocationManager()

    // MARK: - Singleton
    static let shared = Common()
    
    class public func currentLocation() -> CLLocation? {
        
              //let locManager = CLLocationManager()
              locManager.startUpdatingLocation()
              locManager.requestWhenInUseAuthorization()
              var currentLocation: CLLocation?
              if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                  CLLocationManager.authorizationStatus() ==  .authorizedAlways){
                  currentLocation = locManager.location
              }else
              {
                  locManager.stopUpdatingLocation()
                  
              }
              
              return currentLocation
    }
    
    
}

