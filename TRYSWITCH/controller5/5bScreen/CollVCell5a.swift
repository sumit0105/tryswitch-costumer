//
//  CollVCell5a.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 01/10/21.
//

import UIKit

class CollVCell5a: UICollectionViewCell {

    @IBOutlet weak var Bgview: UIView!
    
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        Bgview.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5
        )
    }
}
