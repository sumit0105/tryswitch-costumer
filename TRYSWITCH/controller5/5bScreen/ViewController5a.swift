//
//  ViewController5a.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 01/10/21.
//

import UIKit

class ViewController5a: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var hotspotView:UIView!
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var Btnok: UIButton!
    var arryimg = ["home3","home1","home2","home4","home5","home6"]
    
   
    
    var arraylbl = ["Selling","Buying","Selling","Want to Rent","Renting","Owning"]
    
    var arraylbl2 = ["Buying","","","","Buying","Selling"]
    
    
    var arraylblrow = ["Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since."]
    
    @IBOutlet weak var collView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hotspotView.frame = view.bounds
        view.addSubview(hotspotView)
        hotspotView.isHidden = true
        self.tblview.delegate = self
        self.tblview.dataSource = self
        // Do any additional setup after loading the view.
    }
    
  
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arryimg.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:CollVCell5a = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as!  CollVCell5a
        cell.img.image = UIImage(named: arryimg[indexPath.row])
        
//        cell.layer.cornerRadius = 10
//        cell.layer.borderWidth = 1
//        cell.layer.backgroundColor = UIColor.purple.cgColor
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "PreferVC") as! PreferVC
        self.navigationController?.pushViewController(nextVC, animated: true)
      
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.bounds.size.width/2 - 10
        
        return CGSize(width: w, height: w - 20)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        20
        
    
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10    }
    

    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func next(_ sender: Any) {
        self.hotspotView.isHidden = false
    }
    @IBAction func btnClickOk(_ sender: UIButton) {
        self.hotspotView.isHidden = true
        
       
    }
    
    @IBAction func btnCrros(_ sender: UIButton) {
        self.hotspotView.isHidden = true
       
    }
    
}

extension ViewController5a:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraylbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tblview.separatorStyle = .none
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! I_popupTableViewCell
        cell.lblselling.text = arraylbl[indexPath.row]
        cell.lblrow.text = arraylblrow[indexPath.row]
        cell.lbl2.text = arraylbl2[indexPath.row]
        
//        if cell.lbl2.text == "" {
//            cell.imgarow.image.isHiddin = true
//
//        }
        
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        125
//    }
    

}
