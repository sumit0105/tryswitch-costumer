//
//  URLSession+Extenstion.swift
//  User
//
//  Created by Inmorteltech on 31/01/21.
//

import Foundation

extension URLSession {
    
    func performSynchronously(request: URLRequest) -> (data: Data?, response: URLResponse?, error: Error?) {
        let semaphore = DispatchSemaphore(value: 0)
        
        var data: Data?
        var response: URLResponse?
        var error: Error?
        
        let task = self.dataTask(with: request) {
            data = $0
            response = $1
            error = $2
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
        
        return (data, response, error)
    }
}
