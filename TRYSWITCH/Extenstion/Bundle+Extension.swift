//
//  Bundle+Extension.swift
//  BalvahanApp
//
//  Created by subesh pandey on 14/11/20.
//

import Foundation

extension Bundle {
    var versionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
    var bundleName: String? {
        return infoDictionary?["CFBundleName"] as? String
    }
    
    func appDisplayName() -> String? {
        return infoDictionary?["CFBundleDisplayName"] as? String
    }
    func appBundleIdentifier() -> String? {
        return infoDictionary?["CFBundleIdentifier"] as? String
    }
    
    
}


