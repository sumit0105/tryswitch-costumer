//
//  UIButton+Extenstion.swift
//  BalvahanApp
//
//  Created by subesh pandey on 04/11/20.
//

import Foundation

import UIKit


extension UIButton {
        
    func addShadow() {
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 10
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
        
        self.layer.cornerRadius = 10.0
        
    }
    func setBorder() {
        self.layer.cornerRadius = 10.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.masksToBounds = true
    }
    func setBorder20() {
        self.layer.cornerRadius = 20.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.masksToBounds = true
    }
    func setBorderWithCostomColor(color:UIColor) {
        self.layer.cornerRadius = 0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
    }
    
}

extension UIButton {
    @IBInspectable var adjustFontSizeToWidth: Bool {
        get {
            return ((self.titleLabel?.adjustsFontSizeToFitWidth) != nil)
        }
        set {
            self.titleLabel?.numberOfLines = 1
            self.titleLabel?.adjustsFontSizeToFitWidth = newValue;
            self.titleLabel?.lineBreakMode = .byClipping;
            self.titleLabel?.baselineAdjustment = .alignCenters
            self.titleLabel?.minimumScaleFactor = 0.5

        }
    }
    func checkboxAnimation(closure: @escaping () -> Void){
           guard let image = self.imageView else {return}
           
           UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
               image.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
               
           }) { (success) in
               UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
                   self.isSelected = !self.isSelected
                   //to-do
                   closure()
                   image.transform = .identity
               }, completion: nil)
           }
           
       }
    
}
