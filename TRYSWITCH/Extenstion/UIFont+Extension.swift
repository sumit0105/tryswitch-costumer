//
//  UIFont+Extension.swift
//  GoSpeedyGo
//
//  Created by Inmorteltech on 06/03/21.
//

import Foundation
import UIKit


extension UIFont {
    
    
    func with(style: UIFont.TextStyle, basePointSize: CGFloat, maxPointSize: CGFloat? = nil) -> UIFont {
        if let maxPointSize = maxPointSize {
            return UIFontMetrics(forTextStyle: style).scaledFont(for: self.withSize(basePointSize),
                                                                 maximumPointSize: maxPointSize)
        }
        return UIFontMetrics(forTextStyle: style).scaledFont(for: self.withSize(basePointSize))
    }
}
