//
//  UIViewController+Extension.swift
//  BalvahanApp
//
//  Created by subesh pandey on 06/11/20.
//

import Foundation
import UIKit

extension UIViewController {
    
    func removeChild() {
        self.children.forEach {
            $0.willMove(toParent: nil)
            $0.view.removeFromSuperview()
            $0.removeFromParent()
        }
    }
    
    
    // status bar customize
    func setStatusBarStyle(_ style: UIStatusBarStyle) {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = style == .lightContent ? UIColor.black : .white
//            statusBar.setValue(style == .lightContent ? ColorCompatibility.label : ColorCompatibility.systemBackground, forKey: "foregroundColor")
        }
    }
    
    func setUpStatusBar(_ color : UIColor) {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = color 
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = color
            statusBar?.alpha = 0.5

        }
    }
    
    func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    static let DELAY_SHORT = 1.5
      static let DELAY_LONG = 3.0

      func showToast(_ text: String, delay: TimeInterval = DELAY_LONG) {
          let label = ToastLabel()
          label.backgroundColor = UIColor(white: 0, alpha: 0.5)
          label.textColor = .white
          label.textAlignment = .center
          label.font = UIFont.systemFont(ofSize: 15)
          label.alpha = 0
          label.text = text
          label.clipsToBounds = true
          label.layer.cornerRadius = 20
          label.numberOfLines = 0
          label.textInsets = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
          label.translatesAutoresizingMaskIntoConstraints = false
          view.addSubview(label)

          let saveArea = view.safeAreaLayoutGuide
          label.centerXAnchor.constraint(equalTo: saveArea.centerXAnchor, constant: 0).isActive = true
          label.leadingAnchor.constraint(greaterThanOrEqualTo: saveArea.leadingAnchor, constant: 15).isActive = true
          label.trailingAnchor.constraint(lessThanOrEqualTo: saveArea.trailingAnchor, constant: -15).isActive = true
          label.bottomAnchor.constraint(equalTo: saveArea.bottomAnchor, constant: -30).isActive = true

          UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
              label.alpha = 1
          }, completion: { _ in
              UIView.animate(withDuration: 0.5, delay: delay, options: .curveEaseOut, animations: {
                  label.alpha = 0
              }, completion: {_ in
                  label.removeFromSuperview()
              })
          })
      }
}

class ToastLabel: UILabel {
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }

    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: textInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textInsets.top, left: -textInsets.left, bottom: -textInsets.bottom, right: -textInsets.right)

        return textRect.inset(by: invertedInsets)
    }

    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
}
