//
//  UIImage+Extension.swift
//  BalvahanApp
//
//  Created by subesh pandey on 20/11/20.
//

import Foundation
import UIKit


extension UIImage {
    
    func changeTintColor(color: UIColor) -> UIImage {
        
        var newImage = self.withRenderingMode(.alwaysTemplate)
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, newImage.scale)
        
        color.set()
        
        newImage.draw(in: CGRect(x: 0.0, y: 0.0, width: self.size.width, height: self.size.height))
        
        newImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return newImage
        
    }
    
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    
    func compress(to kb: Int, allowedMargin: CGFloat = 0.2) -> Data {
        guard kb > 10 else { return Data() } // Prevents user from compressing below a limit (10kb in this case).
        let bytes = kb * 1024
        var compression: CGFloat = 1.0
        let step: CGFloat = 0.05
        var holderImage = self
        var complete = false
        while(!complete) {
            guard let data = holderImage.jpegData(compressionQuality: 1.0) else { break }
            let ratio = data.count / bytes
            if data.count < Int(CGFloat(bytes) * (1 + allowedMargin)) {
                complete = true
                return data
            } else {
                let multiplier:CGFloat = CGFloat((ratio / 5) + 1)
                compression -= (step * multiplier)
            }
            guard let newImage = holderImage.resized(withPercentage: compression) else { break }
            holderImage = newImage
        }
        
        return Data()
    }
    
    enum CompressImageErrors: Error {
            case invalidExSize
            case sizeImpossibleToReach
        }
        func compressImage(_ expectedSizeKb: Int, completion : (UIImage,CGFloat) -> Void ) throws {

            let minimalCompressRate :CGFloat = 0.4 // min compressRate to be checked later

            if expectedSizeKb == 0 {
                throw CompressImageErrors.invalidExSize // if the size is equal to zero throws
            }

            let expectedSizeBytes = expectedSizeKb * 1024
            let imageToBeHandled: UIImage = self
            var actualHeight : CGFloat = self.size.height
            var actualWidth : CGFloat = self.size.width
            var maxHeight : CGFloat = 841 //A4 default size I'm thinking about a document
            var maxWidth : CGFloat = 594
            var imgRatio : CGFloat = actualWidth/actualHeight
            let maxRatio : CGFloat = maxWidth/maxHeight
            var compressionQuality : CGFloat = 1
            var imageData:Data = imageToBeHandled.jpegData(compressionQuality: compressionQuality)!
            while imageData.count > expectedSizeBytes {

                if (actualHeight > maxHeight || actualWidth > maxWidth){
                    if(imgRatio < maxRatio){
                        imgRatio = maxHeight / actualHeight;
                        actualWidth = imgRatio * actualWidth;
                        actualHeight = maxHeight;
                    }
                    else if(imgRatio > maxRatio){
                        imgRatio = maxWidth / actualWidth;
                        actualHeight = imgRatio * actualHeight;
                        actualWidth = maxWidth;
                    }
                    else{
                        actualHeight = maxHeight;
                        actualWidth = maxWidth;
                        compressionQuality = 1;
                    }
                }
                let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
                UIGraphicsBeginImageContext(rect.size);
                imageToBeHandled.draw(in: rect)
                let img = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                if let imgData = img!.jpegData(compressionQuality: compressionQuality) {
                    if imgData.count > expectedSizeBytes {
                        if compressionQuality > minimalCompressRate {
                            compressionQuality -= 0.1
                        } else {
                            maxHeight = maxHeight * 0.9
                            maxWidth = maxWidth * 0.9
                        }
                    }
                    imageData = imgData
                }


            }

            completion(UIImage(data: imageData)!, compressionQuality)
        }
    func tintedWithLinearGradientColors(colorsArr: [CGColor]) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale);
        guard let context = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1, y: -1)

        context.setBlendMode(.normal)
        let rect = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)

        // Create gradient
        let colors = colorsArr as CFArray
        let space = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: space, colors: colors, locations: nil)

        // Apply gradient
        context.clip(to: rect, mask: self.cgImage!)
        context.drawLinearGradient(gradient!, start: CGPoint(x: 0, y: 0), end: CGPoint(x: 0, y: self.size.height), options: .drawsAfterEndLocation)
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return gradientImage!
    }
}
 
struct ImageResizer {
    static func resize(image: UIImage, maxByte: Int, completion: @escaping (UIImage?) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let currentImageSize = image.jpegData(compressionQuality: 1.0)?.count else { return completion(nil) }
            
            var imageSize = currentImageSize
            var percentage: CGFloat = 1.0
            var generatedImage: UIImage? = image
            let percantageDecrease: CGFloat = imageSize < 10000000 ? 0.1 : 0.3
            
            while imageSize > maxByte && percentage > 0.01 {
                let canvas = CGSize(width: image.size.width * percentage,
                                    height: image.size.height * percentage)
                let format = image.imageRendererFormat
                format.opaque = true
                generatedImage = UIGraphicsImageRenderer(size: canvas, format: format).image {
                    _ in image.draw(in: CGRect(origin: .zero, size: canvas))
                }
                guard let generatedImageSize = generatedImage?.jpegData(compressionQuality: 1.0)?.count else { return completion(nil) }
                imageSize = generatedImageSize
                percentage -= percantageDecrease
            }
            
            completion(generatedImage)
        }
    }
    
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleToFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleToFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
