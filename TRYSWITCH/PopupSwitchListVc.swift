//
//  PopupSwitchListVc.swift
//  design
//
//  Created by satyam mac on 28/10/21.
//

import UIKit

class PopupSwitchListVc: UIViewController {

    @IBOutlet weak var BgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        BgView.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 21)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func CancelBtn(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    @IBAction func btnListTrySwitch(_ sender: Any) {
       
        let storyB = UIStoryboard(name: "PopUp", bundle: nil)
        let vc: CreateSwitchListVc = storyB.instantiateViewController(withIdentifier: "CreateSwitchListVc") as! CreateSwitchListVc
        
        self.addChild(vc)
        
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
