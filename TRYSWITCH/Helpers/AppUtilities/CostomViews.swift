//
//  CostomViews.swift
//  Caviar
//
//  Created by satyam mac on 23/09/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
import UIKit
import Koloda

let defaultTopOffset: CGFloat = 0
let defaultHorizontalOffset: CGFloat = 0
let defaultHeightRatio: CGFloat = 1.25
let backgroundCardHorizontalMarginMultiplier: CGFloat = 0.25
let backgroundCardScalePercent: CGFloat = 1.5

class CustomKolodaView: KolodaView {

    override func frameForCard(at index: Int) -> CGRect {
        if index == 0 {
            let topOffset: CGFloat = defaultTopOffset
            let xOffset: CGFloat = defaultHorizontalOffset
            let width = (self.frame).width - 2 * defaultHorizontalOffset
            let height = width * defaultHeightRatio
            let yOffset: CGFloat = topOffset
            let frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
            
            return frame
        } else if index == 1 {
            let horizontalMargin = -self.bounds.width * backgroundCardHorizontalMarginMultiplier
            let width = self.bounds.width * backgroundCardScalePercent
            let height = width * defaultHeightRatio
            return CGRect(x: horizontalMargin, y: 0, width: width, height: height)
        }
        return CGRect.zero
    }

}
let defaultTopOffset1: CGFloat = 0
let defaultHorizontalOffset1: CGFloat = 0
let defaultHeightRatio1: CGFloat = 1
let backgroundCardHorizontalMarginMultiplier1: CGFloat = 0.25
let backgroundCardScalePercent1: CGFloat = 1.5

class CustomKolodaView1: KolodaView {

    override func frameForCard(at index: Int) -> CGRect {
        if index == 0 {
            let topOffset: CGFloat = defaultTopOffset1
            let xOffset: CGFloat = defaultHorizontalOffset1
            let width = (self.frame).width - 5 * defaultHorizontalOffset1
            let height = width * defaultHeightRatio1
            let yOffset: CGFloat = topOffset
            let frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
            
            return frame
        } else if index == 1 {
            let horizontalMargin = -self.bounds.width * backgroundCardHorizontalMarginMultiplier1
            let width = self.bounds.width * backgroundCardScalePercent1
            let height = width * defaultHeightRatio1
            return CGRect(x: horizontalMargin, y: 0, width: width, height: height)
        }
        return CGRect.zero
    }

}
