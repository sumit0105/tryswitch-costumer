//
//  AppFonts.swift
//  DannApp
//
//  Created by Aakash Srivastav on 20/04/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit


enum AppFonts : String {
    
    case Bebas_Neue_Pro_Bold_Italic = "BebasNeuePro-Bold-Italic"
    case Bebas_Neue_Pro_Bold = "BebasNeuePro-Bold"
    case Bebas_Neue_Pro_Book_Italic = "BebasNeuePro-Book-Italic"
    case Bebas_Neue_Pro_Book = "BebasNeuePro-Book"
    case Bebas_Neue_Pro_Italic = "BebasNeuePro-Italic"
    case Bebas_Neue_Pro_Light_Italic = "BebasNeuePro-Light-Italic"
    case Bebas_Neue_Pro_Light = "BebasNeuePro-Light"
    case Bebas_Neue_Pro_Regular = "BebasNeuePro-Regular"
    case Bebas_Neue_Pro_Thin_Italic = "BebasNeuePro-Thin-Italic"
    case Bebas_Neue_Pro_Thin = "BebasNeuePro-Thin"
}

extension AppFonts {
    
    func withSize(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: self.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    func withDefaultSize() -> UIFont {
        
        return UIFont(name: self.rawValue, size: 25) ?? UIFont.systemFont(ofSize: 25.0)
    }
    
}

// USAGE : let font = AppFonts.Helvetica.withSize(13.0)
// USAGE : let font = AppFonts.Helvetica.withDefaultSize()
