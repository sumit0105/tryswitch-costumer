//
//  AppTextField.swift
//  DittoFashionMarketBeta
//
//  Created by Bhavneet Singh on 29/11/17.
//  Copyright © 2017 Bhavneet Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

// MARK:- Custom TextField
//=================================
class AppTextField: MarginTextField, UITextFieldDelegate {
    
    // MARK:- Variables
    //===================
    let underline = UIView()
    var errorLabel = UILabel()
    
    var returnKeyHandler: IQKeyboardReturnKeyHandler?
    
    var isSecured: Bool = true {
        didSet{
            self.isSecureTextEntry = !self.isSecureTextEntry
        }
    }
    
    private var pickerData: [String] = []
    
    // MARK:- Initializers
    //======================
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if #available(iOS 13.0, *) {
            self.setupSubviews()
        } else {
            // Fallback on earlier versions
        }
        self.setupKeyboard()
        self.setupBottomBorder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if #available(iOS 13.0, *) {
            self.setupSubviews()
        } else {
            // Fallback on earlier versions
        }
        self.setupKeyboard()
        self.setupErrorLabel()
        self.setupBottomBorder()
    }
    
    // MARK:- Layout Subviews
    //=========================
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setupFrames()
        self.setupErrorLabelFrames()
    }
}

// MARK:- Private Setup Functions
//================================
extension AppTextField {
    
    /// Setup Subviews
    @available(iOS 13.0, *)
    private func setupSubviews() {
        
        self.clipsToBounds = false
        self.borderStyle = .none
        
        self.font = AppFonts.Bebas_Neue_Pro_Regular.withSize(15.0)
        self.textColor = AppColors.textfield_Text
        
        self.rightViewMode = .always
        
        if let rootVC = AppDelegate.shared.window?.rootViewController { 4
            returnKeyHandler = IQKeyboardReturnKeyHandler(controller: rootVC)
            returnKeyHandler?.lastTextFieldReturnKeyType = .done
            returnKeyHandler?.addTextFieldView(self)
        }
    }
    
    /// Setup Error Label
    private func setupErrorLabel() {
        
        self.errorLabel.font = AppFonts.Bebas_Neue_Pro_Regular.withSize(12)
        self.errorLabel.textColor = AppColors.whiteColor
        self.errorLabel.textAlignment = .left
        self.addSubview(self.errorLabel)
    }
    
    /// Setup Error Label Frames
    private func setupErrorLabelFrames() {
        
        self.errorLabel.frame = CGRect(x: 0, y: self.frame.height,
                                       width: self.frame.width, height: self.frame.height/2)
    }
    
    /// Setup Keyboard
    private func setupKeyboard() {
        
        self.autocapitalizationType = .sentences
        self.autocorrectionType = .no
        self.keyboardType = .asciiCapable
        self.keyboardAppearance = .default
    }
    
    /// Setup Frames
    private func setupFrames(){
        
        self.rightView?.frame.size = CGSize(width: rightView!.intrinsicContentSize.width+10, height: bounds.height)
        underline.frame = CGRect(x: 0, y: self.frame.size.height-1, width: self.frame.size.width, height: 1)
    }
    
    /// Setup Bottom Border
    private func setupBottomBorder() {
        
        let height = CGFloat(0.25)
        underline.backgroundColor = UIColor.clear
        underline.frame = CGRect(x: 0, y: self.frame.size.height - height, width: self.frame.size.width, height: height)
        self.addSubview(underline)
        self.clipsToBounds = false
        
        self.layer.cornerRadius = 3
        self.layer.borderWidth = 0.5
        self.layer.borderColor = AppColors.textfield_Border.cgColor
        
    }
    
    /// Show Button Tapped
    @objc private func showButtonTapped(_ btn: UIButton) {
        btn.isSelected = !btn.isSelected
        self.isSecured = !self.isSecured
    }
}


// MARK:- Setup Functions
//==========================
extension AppTextField {
    
    /// First Name TextField
    func setupFirstNameTextField() {
        
        self.keyboardType = .asciiCapable
        self.autocapitalizationType = .words
        self.placeholder = LocalizedString.firstName.localized
    }
    
    /// Middle Name TextField
    func setupMiddleNameTextField() {
        
        self.setupFirstNameTextField()
        self.placeholder = LocalizedString.middleName.localized
    }
    
    /// Last Name TextField
    func setupLastNameTextField() {
        
        self.setupFirstNameTextField()
        self.placeholder = LocalizedString.lastName.localized
    }
    
    /// User Name TextField
    func setupUserNameTextField() {
        
        self.keyboardType = .asciiCapable
        self.autocapitalizationType = .none
        self.placeholder = LocalizedString.userName.localized
    }

    /// Email TextField
    func setupEmailTextField() {
        
        self.keyboardType = .emailAddress
        self.autocapitalizationType = .none
        self.placeholder = LocalizedString.emailAddress.localized
    }
    
    /// Gender TextField
    func setupGenderTextField() {

        self.placeholder = LocalizedString.gender.localized
    }
    
    /// DOB TextField
    func setupDOBTextField(start: Date? = nil, end: Date? = nil, current: Date = Date(), didSelectDate: @escaping ((Date) -> Void)) {
        
        self.placeholder = LocalizedString.dateOfBirth.localized
//        self.createDatePicker(start: start, end: end, current: current, didSelectDate: didSelectDate)
    }
    
    /// Phone Number TextField
    func setupPhoneNumberTextField() {
        
        self.keyboardType = .numberPad
        self.autocapitalizationType = .none
        self.placeholder = LocalizedString.phoneNo.localized
    }

    ///Passowrd Textfield
    func setupPasswordTextField() {
        
        self.placeholder = LocalizedString.password.localized

        self.keyboardType = .asciiCapable
        self.isSecureTextEntry = isSecured
        
        let showButton = UIButton()
        showButton.backgroundColor = .yellow
        showButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        showButton.setImage(UIImage(named: "visibility"), for: .normal)
        showButton.setImage(UIImage(named: "eye"), for: .selected)
        
        
//        showButton.setTitle("Show", for: .normal)
//
//        showButton.setTitle("Hide  ", for: .selected)
        showButton.titleLabel?.font = AppFonts.Bebas_Neue_Pro_Regular.withSize(15)
        showButton.setTitleColor(AppColors.textfield_Text, for: .normal)
        showButton.setTitleColor(AppColors.textfield_Text, for: .selected)
        
        showButton.addTarget(self, action: #selector(self.showButtonTapped(_:)), for: .touchUpInside)
//        self.setButtonToRightView(btn: showButton, selectedImage: nil, normalImage: nil, size: nil)
    }
    
   // CGRect(x: CGFloat(txt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
    
    /// Old Password TextField
    func setupOldPasswordTextField() {
        
        self.setupPasswordTextField()
        self.placeholder = LocalizedString.oldPassword.localized
    }

    /// New Password TextField
    func setupNewPasswordTextField() {
        
        self.setupPasswordTextField()
        self.placeholder = LocalizedString.newPassword.localized
    }
    
    /// Confirm Password TextField
    func setupConfirmPasswordTextField() {
        
        self.setupPasswordTextField()
        self.placeholder = LocalizedString.confirmPassword.localized
    }

    /// City TextField
    func setupCityTextField() {
        
        self.setupFirstNameTextField()
        self.placeholder = LocalizedString.city.localized
    }

    /// Country Code TextField
    func setupCountryCodeTextField() {
        
        self.keyboardType = .numberPad
        self.autocapitalizationType = .none
        self.placeholder = LocalizedString.countryCode.localized
    }

    /// Region TextField
    func setupRegionTextField() {
        
        self.setupFirstNameTextField()
        self.placeholder = LocalizedString.region.localized
    }

    /// Longitude TextField
    func setupLongitudeTextField() {
        
        self.keyboardType = .numberPad
        self.autocapitalizationType = .none
        self.placeholder = LocalizedString.longitude.localized
    }

    /// Latitude TextField
    func setupLatitudeTextField() {
        
        self.keyboardType = .numberPad
        self.autocapitalizationType = .none
        self.placeholder = LocalizedString.latitude.localized
    }

    /// Postal Code TextField
    func setupPostalCodeTextField() {
        
        self.keyboardType = .numberPad
        self.autocapitalizationType = .none
        self.placeholder = LocalizedString.postalCode.localized
    }

}


// MARK:- Picker View Delegate and Datasource
//==============================================
extension AppTextField: UIPickerViewDelegate, UIPickerViewDataSource {

    internal func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    internal func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    
    internal func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 44
    }
    
    internal func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerData[row]
    }
}
class DataDetector {

    private class func _find(all type: NSTextCheckingResult.CheckingType,
                             in string: String, iterationClosure: (String) -> Bool) {
        guard let detector = try? NSDataDetector(types: type.rawValue) else { return }
        let range = NSRange(string.startIndex ..< string.endIndex, in: string)
        let matches = detector.matches(in: string, options: [], range: range)
        loop: for match in matches {
            for i in 0 ..< match.numberOfRanges {
                let nsrange = match.range(at: i)
                let startIndex = string.index(string.startIndex, offsetBy: nsrange.lowerBound)
                let endIndex = string.index(string.startIndex, offsetBy: nsrange.upperBound)
                let range = startIndex..<endIndex
                guard iterationClosure(String(string[range])) else { break loop }
            }
        }
    }

    class func find(all type: NSTextCheckingResult.CheckingType, in string: String) -> [String] {
        var results = [String]()
        _find(all: type, in: string) {
            results.append($0)
            return true
        }
        return results
    }

    class func first(type: NSTextCheckingResult.CheckingType, in string: String) -> String? {
        var result: String?
        _find(all: type, in: string) {
            result = $0
            return false
        }
        return result
    }
}
extension String {
    var detectedLinks: [String] { DataDetector.find(all: .link, in: self) }
    var detectedFirstLink: String? { DataDetector.first(type: .link, in: self) }
    var detectedURLs: [URL] { detectedLinks.compactMap { URL(string: $0) } }
    var detectedFirstURL: URL? {
        guard let urlString = detectedFirstLink else { return nil }
        return URL(string: urlString)
    }
}
