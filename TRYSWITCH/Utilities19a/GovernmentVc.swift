//
//  GovernmentVc.swift
//  TRYSWITCH
//
//  Created by Anubhav on 07/12/21.
//

import UIKit

class GovernmentVc: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    
    @IBOutlet var collectionView:UICollectionView!
     

    var arrayimg = ["g1","g2","f4","p1","g5","g6","g7"]
    var arraylbl = ["US Postal Services","DMV","IRS","Social Security","Voter Registration","Department of Veterans Affairs","US Customs and Immigration Services"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arraylbl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UitilitiesCollectionViewCell
        
        cell.img.image = UIImage(named: arrayimg[indexPath.row])
        cell.lbl.text = arraylbl[indexPath.row]
        
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
//        cell.layer.borderColor = UIColor.purple.cgColor
        
        
        
        return cell
    }

    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let  nextscreen:TelephoneServicesVC = self.storyboard?.instantiateViewController(withIdentifier: "TelephoneServicesVC")as! TelephoneServicesVC
//        self.navigationController?.pushViewController(nextscreen, animated: true)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width)/2 - 10
        return CGSize(width: size, height: (size))

        


        

//        let width = (collectionView.frame.size.width-40)/3
//        let height = width + 40
//        self.returnHeight(count: height)
//        return CGSize(width: width, height: height)


    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        20
    }
    
    @IBAction func btnback (_sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    

}
