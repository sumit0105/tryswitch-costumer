//
//  UitilitiesVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 08/10/21.
//

import UIKit

class UitilitiesVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    
    @IBOutlet var collectionView:UICollectionView!
    @IBOutlet weak var searchBgView:UIView!
    var action:Bool = false
    var selectedIndex = Int ()
    var arrayimg = ["Telephone","T.V","Internet","Gas","Garbeg","Drop","Electricity"]
    var arraylbl = ["Telephone","T.V","Internet","Gas","Garbage recycling","Water","Electricity"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBgView.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.action = false
        self.selectedIndex = Int()
        self.collectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrayimg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UitilitiesCollectionViewCell
        
        cell.img.image = UIImage(named: arrayimg[indexPath.row])
        cell.lbl.text = arraylbl[indexPath.row]
        
        cell.BgView.layer.borderWidth = 1
        cell.BgView.layer.borderColor = UIColor.init(red: 64, green: 17, blue: 114).cgColor
        cell.BgView.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)
        
        if (action){
           
            if selectedIndex == indexPath.row
            {
               
                cell.BgView.layer.borderColor =   UIColor.init(red: 245, green: 66, blue: 8).cgColor
                cell.BgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
            }
            else
            {
        cell.BgView.layer.borderColor = UIColor.init(red: 64, green: 17, blue: 114).cgColor
        cell.BgView.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)
            }
        }


        
        return cell
    }

    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.action = true
        
        selectedIndex = indexPath.row
    
        self.collectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
        let  nextscreen:TelephoneServicesVC = self.storyboard?.instantiateViewController(withIdentifier: "TelephoneServicesVC")as! TelephoneServicesVC
        self.navigationController?.pushViewController(nextscreen, animated: true)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width)/2 - 10
        return CGSize(width: size, height: (size))


    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    @IBAction func btnback (_sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    

}
