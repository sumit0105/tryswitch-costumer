//
//  FindRealtorsVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 12/10/21.
//

import UIKit

class FindRealtorsVC1: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var txtSearchTf: TextField!
    
    @IBOutlet var tblView : UITableView!
    
   
    

    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearchTf.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)
//        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)

        self.tblView.delegate = self
        self.tblView.dataSource = self
       
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FindRealtorsCell1
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = FindRealtorsVC.instantiate(fromAppStoryboard:.Main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        150
    }
}
