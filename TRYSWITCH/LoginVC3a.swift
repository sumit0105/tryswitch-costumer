//
//  LoginVC3a.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 30/09/21.
//

import UIKit
import SideMenuSwift
class LoginVC3a: UIViewController {
    
    @IBOutlet weak var btncornerR: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.topRoundCorner(radius: 60, color: .white, borderWidth: 1)
        btncornerR.layer.cornerRadius = 20
        btncornerR.clipsToBounds = true
        
        btncornerR.layer.shadowRadius = 10
        btncornerR.layer.shadowOpacity = 1.0
        

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func loginBtn(_ sender: Any) {
        
        let doyoucontroller = DoYouVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(doyoucontroller, animated: true)
        
//        let storyB = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil)
//
//        let doyoucontroller = storyB.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
//        self.navigationController?.pushViewController(doyoucontroller, animated: true)
    }
    
    
    @IBAction func btnforgotpasswors(_ sender: UIButton) {
        
        let forgot =  self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC") as! ForgotVC
        self.navigationController?.pushViewController(forgot, animated: true)
    }
    
    @IBAction func signupBtn(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SingupVC") as! SingupVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    
}
