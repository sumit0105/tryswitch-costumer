//
//  PropertyVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 07/10/21.
//

import UIKit
import DropDown

class PropertyVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var typeDropdown: UITextField!
    
    @IBOutlet var btncor: UIButton!
    
    @IBOutlet weak var coLL_View: UICollectionView!
    @IBOutlet weak var txt_Dis: UITextView!
    let dropDown = DropDown()
    
    let dropdownvalue = ["allpr","mango","orange"]

    @IBOutlet var txt_border: [UITextField]!
    override func viewDidLoad() {
        super.viewDidLoad()
        coLL_View.dataSource = self
        coLL_View.delegate = self
        self.typeDropdown.delegate = self
        txt_Dis.setBorderColor(borderColor: .gray, cornerRadiusBound: 15)
        for item in txt_border {
            item.setBorderColor(borderColor: .gray, cornerRadiusBound: item.bounds.height/2)
        }
    
        
        btncor.layer.cornerRadius = 20
        btncor.clipsToBounds = true
        
        btncor.layer.shadowRadius = 10
        btncor.layer.shadowOpacity = 1.0
        
//        setDropDown()
    }
    
    
    @IBAction func btnnext (_ sender: UIButton){
        let submit = storyboard?.instantiateViewController(withIdentifier: "H6propertyDetial") as! H6propertyDetial
        
        self.navigationController?.pushViewController(submit, animated: true)
    }
    
    
    
    @IBAction func btnback (_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    

//    func setDropDown(){
//        dropDown.anchorView = typeDropdown
//        dropDown.dataSource = ["allpr","mango","orange"]
//        dropDown.sectionAction = \{[unowned self] (Index: Int, itme: String) in print("Selected itme: \(itme) at index: \(Index)")
//
//            self.selected = itme
//            self.projectPriorityTextField.text = itme
//        }
//       dropDown.show
//    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
}
extension PropertyVC:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropertCollectionViewCell", for: indexPath) as! PropertCollectionViewCell
       
        cell.propertyImg.image = UIImage(named: "9")
        if indexPath.item == 2 {
            cell.propertyImg.isHidden = true
        }
        cell.layer.cornerRadius = 5
        
        
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let size = CGSize(width: (collectionView.bounds.size.width/3)-9, height: collectionView.bounds.size.height - 10
                          
        )
        return size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        9
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
    }
}
class PropertCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var propertyImg:UIImageView!
    
}
