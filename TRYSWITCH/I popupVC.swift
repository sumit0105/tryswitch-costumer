//
//  I popupVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 04/10/21.
//

import UIKit

class I_popupVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblview: UITableView!
    
    @IBOutlet weak var Btnok: UIButton!
    
    
    
    var arraylbl = ["Selling","Buying","selling","Want to rent","renting","Owning"]
    
    var arraylbl2 = ["buying","","","","Buying","selling"]
    
    
    var arraylblrow = ["Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since.","Lorem Ipsum is semple dummy text of the printingand typesetting indrusty. Lorem Ipsum has been the indrusty's standard dummy text ever since."]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        Btnok.layer.cornerRadius = 15
        Btnok.clipsToBounds = true
        
        Btnok.layer.shadowRadius = 10
        Btnok.layer.shadowOpacity = 1.0
        
        
        self.tblview.delegate = self
        self.tblview.dataSource = self
//        tblview.separatorStyle = .none

        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraylbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tblview.separatorStyle = .none
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! I_popupTableViewCell
        cell.lblselling.text = arraylbl[indexPath.row]
        cell.lblrow.text = arraylblrow[indexPath.row]
        cell.lbl2.text = arraylbl2[indexPath.row]
        
//        if cell.lbl2.text == "" {
//            cell.imgarow.image.isHiddin = true
//            
//        }
        
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        125
//    }
    
    @IBAction func btnClickOk(_ sender: UIButton) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "PreferVC") as! PreferVC
        self.navigationController?.pushViewController(nextVC, animated: true)
        
        
        
        
    }
    
    @IBAction func btnCrros(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
