//
//  WishListVc.swift
//  design
//
//  Created by satyam mac on 28/10/21.
//

import UIKit

class WishListVc: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "AddToMyWishlistVc", bundle: nil), forCellReuseIdentifier: "AddToMyWishlistVc")
    }
    @IBAction func btndismis(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    


}
extension WishListVc:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "AddToMyWishlistVc") as! AddToMyWishlistVc
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        65
        
    }
}
