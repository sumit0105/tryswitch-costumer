//
//  EditProfileVc.swift
//  design
//
//  Created by satyam mac on 27/10/21.
//

import UIKit

class EditProfileVc: UIViewController {
    @IBOutlet weak var bgView: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)

       
    }
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }



}
