//
//  TelephoneServicesVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 08/10/21.
//

import UIKit

class TelephoneServicesVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet var btn: UIButton!
 
    
    @IBOutlet var collView:UICollectionView!
    @IBOutlet weak var searchBgView:UIView!
    
    var arrayimg = ["logo-1","logo-2","logo-3","logo-4","logo-5","logo-6"]
//    var arraylbl = [""]
    var action:Bool = false
    var selectedIndex = Int ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBgView.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)
        btn.layer.cornerRadius = 20
        btn.clipsToBounds = true
        
        btn.layer.shadowRadius = 10
        btn.layer.shadowOpacity = 1.0
        
        self.collView.delegate = self
        self.collView.dataSource = self
     
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.action = false
        self.selectedIndex = Int()
        self.collView.reloadData()
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrayimg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TelephoneColleViewCell

        cell.img.image = UIImage(named: arrayimg[indexPath.row])
         
      
       
      
//            cell.BgView.layer.borderWidth = 1
//            
//            cell.BgView.layer.borderColor = UIColor.init(red: 64, green: 17, blue: 114).cgColor
            cell.BgBtn.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)

        cell.BgBtn.addTarget(self, action: #selector(BgBtnClicked(sender:)), for: .touchUpInside)
     
               return cell
       
       }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width  )/3 - 5
        return CGSize(width: size, height: (size))

  
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        5
    }
    @IBAction func addCheckListBtn(_ sender: Any) {
     let vc = WishListVc.instantiate(fromAppStoryboard: .PopUp)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func btnback (_sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
        
    }
 
 
@objc func BgBtnClicked (sender: UIButton ) {
    if sender.isSelected {
        sender.isSelected = false
       
        
    }else{
        sender.isSelected = true
        
    }
}
}
