//
//  PopularLoalitiesCollCell.swift
//  TRYSWITCH
//
//  Created by Anubhav on 28/10/21.
//

import UIKit

class PopularLoalitiesCollCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5
        )
        img.topRoundCorner(radius: 10, color: .white, borderWidth: 0)
        // Initialization code
    }

}
extension UIView {
    
    func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat) {
        
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = offSet
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = shadowRadius
    }
}
