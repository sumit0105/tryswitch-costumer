//
//  TrySwitchListContainerVcViewController.swift
//  TRYSWITCH
//
//  Created by Anubhav on 30/11/21.
//

import UIKit
import MXSegmentedControl
class TrySwitchListContainerVc: UIViewController {
    @IBOutlet var segmentedControl3: MXSegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControl3.append(title: "All")
            .set(titleColor:UIColor(red: 65, green: 0, blue: 99), for: .selected)
        
        segmentedControl3.append(title: "My")
            .set(titleColor: UIColor(red: 65, green: 0, blue: 99), for: .selected)
        segmentedControl3.append(title: "Shared")
            .set(titleColor: UIColor(red: 65, green: 0, blue: 99), for: .selected)
        segmentedControl3.addTarget(self, action: #selector(changeIndex(segmentedControl:)), for: .valueChanged)
    }
    
    @objc func changeIndex(segmentedControl: MXSegmentedControl) {
                
        if let segment = segmentedControl.segment(at: segmentedControl.selectedIndex) {
            segmentedControl.indicator.boxView.backgroundColor = segment.titleColor(for: .selected)
            segmentedControl.indicator.lineView.backgroundColor = segment.titleColor(for: .selected)
        }
    }
    @IBAction func back_btn(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
