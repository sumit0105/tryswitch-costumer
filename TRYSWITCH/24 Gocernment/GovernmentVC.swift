//
//  GovernmentVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 20/10/21.
//

import UIKit

class GovernmentVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var arrayimg:[UIImage] = [#imageLiteral(resourceName: "37"),#imageLiteral(resourceName: "38"),#imageLiteral(resourceName: "35 (1).png"),#imageLiteral(resourceName: "25"),#imageLiteral(resourceName: "39"),#imageLiteral(resourceName: "40"),#imageLiteral(resourceName: "41")]
    
    var arrayname = ["Us postal","DMV","IRS","Social Security","Voter Registration","Department of Voterans Affaire","US Customs and Immigration Services"]
  
    
    
    
    @IBOutlet weak var colectionViewGove: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.colectionViewGove.delegate = self
        self.colectionViewGove.dataSource = self
        
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
               var width = UIScreen.main.bounds.width
               layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
           width = width - 10
           layout.itemSize = CGSize(width: width / 3, height: width / 3)
               layout.minimumInteritemSpacing = 5
               layout.minimumLineSpacing = 20
        colectionViewGove.collectionViewLayout = layout
        
        
     
       
    }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayimg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:GovColleViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GovColleViewCell
        
        cell.img.image = arrayimg[indexPath.row]
        cell.lblname.text = arrayname[indexPath.row]
        
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.purple.cgColor
        
        return cell
    }
    
    
    
    
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//        {
//           return CGSize(width: 180.0, height: 130.0)
//
////        let width  = (view.frame.width-50)/3
////        return CGSize(width: width, height: width)
//
//
//
//
//        }
    
    
//
//    func GovColleViewCell(_ collectionView: UICollectionView, layout collectionViewLayout:
//    UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//      let padding: CGFloat =  80
//      let collectionViewSize = colectionViewGove.frame.size.width - padding
//      return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
//    }
//
    
    
}
