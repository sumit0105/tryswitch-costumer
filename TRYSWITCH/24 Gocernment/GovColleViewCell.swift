//
//  GovColleViewCell.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 20/10/21.
//

import UIKit

class GovColleViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    
}
