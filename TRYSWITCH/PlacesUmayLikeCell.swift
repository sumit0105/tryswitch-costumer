//
//  PlacesUmayLikeCell.swift
//  TRYSWITCH
//
//  Created by Anubhav on 28/10/21.
//

import UIKit

class PlacesUmayLikeCell: UICollectionViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
        img.topRoundCorner(radius: 10, color: .white, borderWidth: 0)
    }

}
