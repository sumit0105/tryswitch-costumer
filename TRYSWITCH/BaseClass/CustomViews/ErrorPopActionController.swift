//
//  ErrorPopActionController.swift
//  Loot
//
//  Created by satyam mac on 10/03/21.
//

import UIKit

protocol ErrorPopUpProtocol {
    func handleAction(action: Bool)
}

class ErrorPopActionController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    static var name: String { "Intrinsic And Fullscreen" }
    

    static let identifier = "ErrorPopUpActionViewController"
    
    var delegate: ErrorPopUpProtocol?
    
    //MARK:- outlets for the view controller
    @IBOutlet weak var dialogBoxView: UIView!
    @IBOutlet weak var panView: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    
    
  
  
    var arrayDs:[String] = ["Front Image","Back Image","Left Image","Right Image"]
   
    @IBOutlet weak var serviceListCollectionView:UICollectionView!
    //MARK:- lifecyle methods for the view controller
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceListCollectionView.delegate = self
        serviceListCollectionView.dataSource = self
        serviceListCollectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "ServiceCollectionViewCell")
        editButton.setBorder()
        removeButton.setBorder()
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
   
    //MARK:- functions for the viewController
//    static func openDemo(from parent: UIViewController, in view: UIView?) {
//        let useInlineMode = view != nil
//
//        let controller = (UIStoryboard(name: "CustomView", bundle: nil).instantiateViewController(withIdentifier: "ErrorPopUpActionViewController") as? ErrorPopActionController)!
//
//        let options = SheetOptions(
//            useFullScreenMode: false,
//            useInlineMode: useInlineMode)
//        let sheet = SheetViewController(controller: controller, sizes: [.fullscreen, .intrinsic], options: options)
//        sheet.minimumSpaceAbovePullBar = 44
//
//        addSheetEventLogging(to: sheet)
//
//        if let view = view {
//            sheet.animateIn(to: view, in: parent)
//        } else {
//            parent.present(sheet, animated: true, completion: nil)
//        }
//    }
    static func showPopup(parentVC: UIViewController){
        //creating a reference for the dialogView controller
        if let popupViewController = UIStoryboard(name: "CustomView", bundle: nil).instantiateViewController(withIdentifier: "ErrorPopUpActionViewController") as? ErrorPopActionController {
            popupViewController.modalPresentationStyle = .automatic
            popupViewController.modalTransitionStyle = .coverVertical
            
            //setting the delegate of the dialog box to the parent viewController
            popupViewController.delegate = parentVC as? ErrorPopUpProtocol

            //presenting the pop up viewController from the parent viewController
            parentVC.present(popupViewController, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrayDs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
        cell.serviceLbl.text = self.arrayDs[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let size = CGSize(width: (collectionView.bounds.size.width/3)-9, height: collectionView.bounds.size.height - 10
                          
        )
        return size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        9
    }
}
