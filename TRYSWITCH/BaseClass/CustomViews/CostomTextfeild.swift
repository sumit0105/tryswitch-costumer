//
//  CostomTextfeild.swift
//  Inspecto
//
//  Created by satyam mac on 27/05/21.
//

import Foundation



import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields


class Constants{
    struct Color {
        static let SavanColor = UIColor.init(netHex: 0x30B55A)
 
    }
}
class CustomOutlinedTxtField: UIView {

var textField: MDCOutlinedTextField!

@IBInspectable var placeHolder: String!
@IBInspectable var value: String!
@IBInspectable var primaryColor: UIColor! = .lightGray

override open func draw(_ rect: CGRect) {
    super.draw(rect)

    textField.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)

}
open override func awakeFromNib() {
    super.awakeFromNib()
    setUpProperty()
}
func setUpProperty() {
    //Change this properties to change the propperties of text
   textField = MDCOutlinedTextField(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
      textField.label.text = placeHolder
     textField.setNormalLabelColor(.lightGray, for: .normal)
     textField.setTextColor(.white, for: .normal)
     textField.setTextColor(.white, for: .editing)

    textField.setOutlineColor(.darkGray, for: .normal)
    textField.setOutlineColor(.lightGray, for: .editing)
    textField.setFloatingLabelColor(.lightGray, for: .normal)
    textField.setFloatingLabelColor(.lightGray, for: .editing)
    
    textField.sizeToFit()
    
     
    self.addSubview(textField)
}
}

class CustomOutlinedTxtFieldDropDown: UIView {

var textField: MDCOutlinedTextField!

@IBInspectable var placeHolder: String!
@IBInspectable var value: String!
@IBInspectable var primaryColor: UIColor! = .lightGray

override open func draw(_ rect: CGRect) {
    super.draw(rect)

    textField.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)

}
open override func awakeFromNib() {
    super.awakeFromNib()
    setUpProperty()
}
func setUpProperty() {
    //Change this properties to change the propperties of text
   textField = MDCOutlinedTextField(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
      textField.label.text = placeHolder
     textField.setNormalLabelColor(.lightGray, for: .normal)
     textField.setTextColor(.white, for: .normal)
     textField.setTextColor(.white, for: .editing)

    textField.setOutlineColor(.darkGray, for: .normal)
    textField.setOutlineColor(.lightGray, for: .editing)
    textField.setFloatingLabelColor(.lightGray, for: .normal)
    textField.setFloatingLabelColor(.lightGray, for: .editing)
    
    textField.sizeToFit()
    
     
    self.addSubview(textField)
}
}
