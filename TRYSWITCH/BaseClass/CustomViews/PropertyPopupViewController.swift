//
//  PropertyPopupViewController.swift
//  CustomDialogBox
//
//  Created by satyam  on 01/04/20.
//  Copyright © 2020 satyam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FittedSheets

protocol PopUpProtocolAction {
    
    func handleAction(action: Bool)
}

class PropertyPopupViewController:UIViewController, Demoable {
    
    static var name: String = {"hgcycyt"}()
    static var imageData:[UIImage]? = []
    static var dict :[String:String]!
    static let identifier = "PropertyPopupViewController"
    @IBOutlet var tblView:UITableView!
    //MARK:- outlets for the viewController
   
    var delegate: PopUpProtocolAction?
    //MARK:- lifecyle methods for the view controller
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
    
    }
    
    @IBAction func publishListBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewAddressVC") as! NewAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    static func openDemo(from parent: UIViewController, in view: UIView?) {
        let useInlineMode = view != nil
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PropertyPopupViewController") as! PropertyPopupViewController
         
        let options = SheetOptions(
            useFullScreenMode: false,
            useInlineMode: useInlineMode)
        let sheet = SheetViewController(controller: controller, sizes: [.percent(0.6),.fullscreen], options: options)
//        sheet.hasBlurBackground = true
        sheet.overlayView.isOpaque = true
        sheet.gripSize = CGSize(width: 150, height: 8)
        sheet.minimumSpaceAbovePullBar = 100
        sheet.pullBarBackgroundColor = .white
        sheet.dismissOnPull = false
        sheet.dismissOnOverlayTap = false
        addSheetEventLogging(to: sheet)
        
        if let view = view {
            sheet.animateIn(to: view, in: parent)
        } else {
            parent.present(sheet, animated: true, completion: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
//        self.dialogBoxView.popIn()
    }
  
    static func showPopup(parentVC: UIViewController){
        
        //creating a reference for the dialogView controller
        if let popupViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "") as? PropertyPopupViewController {
            popupViewController.modalPresentationStyle = .custom
            popupViewController.modalTransitionStyle = .crossDissolve
            
            //presenting the pop up viewController from the parent viewController

            parentVC.present(popupViewController, animated: true)
        }
    }
}
extension PropertyPopupViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "propertyCell") as! propertyCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 690
    }
    
    
   
}


class propertyCell:UITableViewCell{
    
}
