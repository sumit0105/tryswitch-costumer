//
//  H6propertyDetial.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 14/10/21.
//

import UIKit
import FittedSheets
class H6propertyDetial: SimpleDemo {
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        PropertyPopupViewController.openDemo(from: self, in: self.view)
        let useInlineMode = view != nil
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PropertyPopupViewController") as! PropertyPopupViewController
       
    let sheet = SheetViewController(
        controller: controller,
        sizes: [.percent(0.6),.percent(0.9) ],
        options: SheetOptions(useInlineMode: useInlineMode))
    sheet.allowPullingPastMaxHeight = false
    sheet.allowPullingPastMinHeight = false
    
    sheet.dismissOnPull = false
    sheet.dismissOnOverlayTap = false
    sheet.overlayColor = UIColor.clear
    
    sheet.contentViewController.view.layer.shadowColor = UIColor.black.cgColor
    sheet.contentViewController.view.layer.shadowOpacity = 0.1
    sheet.contentViewController.view.layer.shadowRadius = 10
    sheet.allowGestureThroughOverlay = true
    
    H6propertyDetial.addSheetEventLogging(to: sheet)
    
    if let view = view {
        sheet.animateIn(to: view, in: self)
    } else {
        self.present(sheet, animated: true, completion: nil)
    }
    }
    
   
    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
