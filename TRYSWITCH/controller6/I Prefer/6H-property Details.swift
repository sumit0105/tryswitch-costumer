//
//  6H-property Details.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 11/10/21.
//

import UIKit

class _H_property_Details: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    var imgarry: [UIImage] = [#imageLiteral(resourceName: "1"),#imageLiteral(resourceName: "2"),#imageLiteral(resourceName: "1")]

    @IBOutlet var colletionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.colletionView.delegate = self
        self.colletionView.dataSource = self
        
    }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        imgarry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ColleViewCell6h
        cell.img.image = imgarry[indexPath.row]
        
        
        return cell
    }
   

}
