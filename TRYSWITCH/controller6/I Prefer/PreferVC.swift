//
//  PreferVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 01/10/21.
//

import UIKit

class PreferVC: UIViewController {
    
    @IBOutlet weak var CollBgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var RentingBtn: UIButton!
    @IBOutlet weak var SellingBtn: UIButton!
    @IBOutlet weak var ByuingBtn: UIButton!
    @IBOutlet var btnCornerR: UIButton!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet var btnBorder: [UIButton]!
    @IBOutlet var tfBorder: [UITextField]!
    @IBOutlet var unit_lbl:UITextField!
    @IBOutlet var unit_lbl1:UITextField!
    var counterValue = 1
    var counterValue1 = 1
    var action:Bool = false
    var selectedIndex = Int ()
    var araayDs:[String] = []
    var imageDs:[String]  = []
    var imgarray = ["House","Townhouse","Cando","Multi-family","Manufactured","Co-op","Land"]
    var lblarray = ["House","Townhouse","Cando","Multi-family","Manufactured","Co-op","Land"]
    var imgarray1 = ["House","Townhouse","Cando"]
    var lblarray1 = ["House","Townhouse","Cando"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CollBgViewHeight.constant = 380
        for i in 0..<btnBorder.count{
            btnBorder[i].setBorderWithCostomColor(color: UIColor.init(red: 64, green: 17, blue: 114))
        }
        for i in 0..<tfBorder.count{
            tfBorder[i].setBorderColor(borderColor: UIColor.init(red: 64, green: 17, blue: 114), cornerRadiusBound: 5)
        }
        btnCornerR.layer.cornerRadius = 20
        btnCornerR.clipsToBounds = true
        setBorderBack(sendar: ByuingBtn)
        clearBorderBack(sendar: RentingBtn)
        clearBorderBack(sendar: SellingBtn)
        btnCornerR.layer.shadowRadius = 10
        btnCornerR.layer.shadowOpacity = 1.0
        
        for i in lblarray{
            araayDs.append(i)
        }
        for i in imgarray{
            imageDs.append(i)
        }
        
        self.collView.delegate = self
        self.collView.dataSource = self
         

    }
    

    @IBAction func tabBtn(_ sender: UIButton) {
        
         if sender.tag == 0{
             CollBgViewHeight.constant = 380
             setBorderBack(sendar: ByuingBtn)
             clearBorderBack(sendar: RentingBtn)
             clearBorderBack(sendar: SellingBtn)
             araayDs.removeAll()
             imageDs.removeAll()
             for i in lblarray{
                 araayDs.append(i)
             }
             for i in imgarray{
                 imageDs.append(i)
             }
             self.action = false
             selectedIndex = Int()
             collView.reloadData()
            
        }else if sender.tag == 1 {
            CollBgViewHeight.constant = 380
            setBorderBack(sendar: SellingBtn)
            clearBorderBack(sendar: RentingBtn)
            clearBorderBack(sendar: ByuingBtn)
            araayDs.removeAll()
            imageDs.removeAll()
            for i in lblarray{
                araayDs.append(i)
            }
            for i in imgarray{
                imageDs.append(i)
            }
            self.action = false
            selectedIndex = Int()
            collView.reloadData()
            
        }else if sender.tag == 2 {
            CollBgViewHeight.constant = 250
            setBorderBack(sendar: RentingBtn)
            clearBorderBack(sendar: ByuingBtn)
            clearBorderBack(sendar: SellingBtn)
            araayDs.removeAll()
            imageDs.removeAll()
            for i in lblarray1{
                araayDs.append(i)
            }
            for i in imgarray1{
                imageDs.append(i)
            }
            self.action = false
            selectedIndex = Int()
            collView.reloadData()
            
        }
        
    }
    func setBorderBack (sendar:UIButton){
        sendar.backgroundColor = UIColor.init(red: 245, green: 66, blue: 8)
        sendar.setBorderColor(borderColor: UIColor.init(red: 245, green: 66, blue: 8), cornerRadiusBound: 20)
        sendar.setTitleColor(.white, for: .normal)
    }
    func clearBorderBack (sendar:UIButton){
        sendar.backgroundColor = UIColor.white
        sendar.setBorderColor(borderColor: UIColor.init(red: 245, green: 66, blue: 8), cornerRadiusBound: 20)
        sendar.setTitleColor(UIColor.init(red: 245, green: 66, blue: 8), for: .normal)
    }
 
    @IBAction func btn_plusTap(_ sender: UIButton) {
            counterValue += 1;
                   self.unit_lbl.text = "\(counterValue)"
        }
        @IBAction func btn_minusTap(_ sender: UIButton) {
            if(counterValue != 1){
                        counterValue -= 1;
                    }
                    self.unit_lbl.text = "\(counterValue)"
                   
        }
    @IBAction func btn_plusTap1(_ sender: UIButton) {
            counterValue1 += 1;
                   self.unit_lbl1.text = "\(counterValue1)"
        }
        @IBAction func btn_minusTap1(_ sender: UIButton) {
            if(counterValue1 != 1){
                        counterValue1 -= 1;
                    }
                    self.unit_lbl1.text = "\(counterValue1)"
                   
        }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextBtn(_ sender: Any) {
        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "MapkitVC") as! MapkitVC
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
extension PreferVC :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return araayDs.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NameCollectionViewCell
       
        
        cell.collimg.image = UIImage(named: imageDs[indexPath.row])
        cell.colllbl.text = araayDs[indexPath.row]
        //cell.BgView.layer.borderWidth = 1
//        cell.BgView.layer.borderColor = UIColor.init(red: 64, green: 17, blue: 114).cgColor
        cell.BgBtn.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)
        
        cell.BgBtn.addTarget(self, action: #selector(BgBtnClicked(sender:)), for: .touchUpInside)
        
        
        
//        if (action){
//
//            if selectedIndex == indexPath.row
//            {
//
//                cell.BgView.layer.borderColor =   UIColor.init(red: 245, green: 66, blue: 8).cgColor
//                cell.BgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
//            }
//            else
//            {
//        cell.BgView.layer.borderColor = UIColor.init(red: 64, green: 17, blue: 114).cgColor
//        cell.BgView.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)
//    }
        
        return cell
    }
    
    
    
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.action = true
//
//        selectedIndex = indexPath.row
//
//        self.collView.reloadData()
//
//    }
    
    @objc func BgBtnClicked (sender: UIButton ) {
        
        if sender.isSelected {
            sender.isSelected = false
        }else{
            sender.isSelected = true
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width )/3 - 5
        return CGSize(width: size, height: (size))

  
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        5
    }
}


