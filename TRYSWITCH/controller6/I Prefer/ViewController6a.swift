//
//  ViewController6a.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 05/10/21.
//

import UIKit
import CarbonKit


class ViewController6a: UIViewController,CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var cantainerView: UIView!
    
//    var controllername = ["Buying","Selling","Renting"]
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()

    override func viewDidLoad() {
        super.viewDidLoad()
        
       carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: ["Buying","Selling","Renting"], delegate: self)
        carbonTabSwipeNavigation.view.frame = self.cantainerView.bounds
        cantainerView.addSubview(carbonTabSwipeNavigation.view)
        addChild(carbonTabSwipeNavigation)
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 3, forSegmentAt: 0 )
        
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 3, forSegmentAt: 1 )
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 3, forSegmentAt: 2 )
        
        carbonTabSwipeNavigation.setTabBarHeight(44)
        
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.darkGray)
        
        carbonTabSwipeNavigation.setNormalColor(.darkGray)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.blue)
        carbonTabSwipeNavigation.setCurrentTabIndex(0, withAnimation: true)
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.red)
        
        
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor.white
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        guard let storybord  = storyboard else { return UIViewController() }
        if index == 0{
            
            return storybord.instantiateViewController(withIdentifier: "PreferVC")
        }
        else if index == 1 {
            return storybord.instantiateViewController(withIdentifier: "ViewController6c")
            
            
        }
        
        else{
            return storybord.instantiateViewController(withIdentifier: "ViewController6d")
        }

    }
    
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}


    
    


