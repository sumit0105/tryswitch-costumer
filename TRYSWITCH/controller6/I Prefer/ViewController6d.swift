//
//  ViewController6d.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 05/10/21.
//

import UIKit

class ViewController6d: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
   
    @IBOutlet var btncornerR: UIButton!
    @IBOutlet var unit_lbl:UITextField!
    @IBOutlet var unit_lbl1:UITextField!
    @IBOutlet var btnBorder: [UIButton]!
    @IBOutlet weak var collView: UICollectionView!
    
    var counterValue = 1
    var counterValue1 = 1
    var imgarray = ["House","Townhouse","Cando"]
    var lblarray = ["House","Townhouse","Cando"]
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collView.delegate = self
        self.collView.dataSource = self
        
        for i in 0..<btnBorder.count{
            btnBorder[i].setBorderWithCostomColor(color: .black)
        }
        
        btncornerR.layer.cornerRadius = 20
        btncornerR.clipsToBounds = true
        
        btncornerR.layer.shadowRadius = 10
        btncornerR.layer.shadowOpacity = 1.0
        

       
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgarray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! nameColleViewCell
        
        cell.img.image = UIImage(named: imgarray[indexPath.row])
        cell.lbl.text = lblarray[indexPath.row]
        
        
        
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.purple.cgColor
        
        
        return cell
    }
    
    
    
    
func collectionView(_ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath) -> CGSize {
let size = (collectionView.frame.size.width)/3 - 10
return CGSize(width: size, height: (size - 10))






//        let width = (collectionView.frame.size.width-40)/3
//        let height = width + 40
//        self.returnHeight(count: height)
//        return CGSize(width: width, height: height)


}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
10
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
10
}
    
    @IBAction func btnnext (_sender:UIButton){
        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "MapkitVC") as! MapkitVC
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func btn_plusTap(_ sender: UIButton) {
            counterValue += 1;
                   self.unit_lbl.text = "\(counterValue)"
        }
        @IBAction func btn_minusTap(_ sender: UIButton) {
            if(counterValue != 1){
                        counterValue -= 1;
                    }
                    self.unit_lbl.text = "\(counterValue)"
                   
        }
    @IBAction func btn_plusTap1(_ sender: UIButton) {
            counterValue1 += 1;
                   self.unit_lbl1.text = "\(counterValue1)"
        }
        @IBAction func btn_minusTap1(_ sender: UIButton) {
            if(counterValue1 != 1){
                        counterValue1 -= 1;
                    }
                    self.unit_lbl1.text = "\(counterValue1)"
                   
        }
        
    }

