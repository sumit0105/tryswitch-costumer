//
//  22PersonalScreenVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 14/10/21.
//

import UIKit

class _2PersonalScreenVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    var arrayimg: [UIImage] = [#imageLiteral(resourceName: "istockphoto-1158713117-612x612"),#imageLiteral(resourceName: "9"),#imageLiteral(resourceName: "img3")]
    var arraylblname = ["sachin","amit","deepak"]
    
    
    @IBOutlet weak var ColleView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ColleView.dataSource = self
        self.ColleView.delegate = self
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayimg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell: _2ColleViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! _2ColleViewCell
        
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! _2ColleViewCell
//
        cell.img.image = arrayimg[indexPath.row]
        cell.lblname.text = arraylblname[indexPath.row]
        
        return cell
        
        
    }
//
//    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize
//        {
//        let cellSize:CGSize = CGSize(width: self.ColleView.frame.width/2, height: 250)
//            return cellSize
//        }
    
    
    
//    func collectionView(collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAtIndexPath indexPath:  NSIndexPath) -> CGSize {
//
//        return CGSize(width: 250, height:400)
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width-80)/2
        let height = width + 50

        return CGSize(width: width, height: height)
    }
    
}

