//
//  HomeVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 11/10/21.
//

import UIKit

class TrySwitchListPopUp: UIViewController {
    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var txtView: UIView!
    @IBOutlet weak var mainView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnDismiss.layer.borderWidth = 1.0
        self.btnDismiss.layer.borderColor = UIColor.lightGray.cgColor
        self.btnDismiss.clipsToBounds = true
        self.btnDismiss.layer.masksToBounds = true
        self.btnDismiss.layer.cornerRadius = self.btnDismiss.frame.height / 2
        //self.btnDismiss.layer.cornerRadius = self.btnDismiss.height / 2
//        self.btnDismiss.btnDismiss.layer.cornerRadius = self.height / 2
        
        
        
        self.txtView.layer.borderWidth = 1.0
        self.txtView.layer.borderColor = UIColor.lightGray.cgColor
        self.txtView.clipsToBounds = true
        self.txtView.layer.masksToBounds = true
        self.txtView.layer.cornerRadius = 3
        
        
        self.mainView.shadowSetFunc()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)

       
        // Do any additional setup after loading the view.
    }
    
    
    
   
    
    

    @IBAction func btnBack(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }

}
