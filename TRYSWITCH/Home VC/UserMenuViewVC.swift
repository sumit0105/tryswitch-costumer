//
//  HomeVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 11/10/21.
//

import UIKit

class UserMenuViewVC: UIViewController {
    
    //MARK:- Variable
    let arrMenuTitle = ["Real Estate Agents","Service Provider","My Real Estate Journey","Move In and Move Out Checklist","My Real Estate Calendar","Real Estate Glossary","Invite Friends","Refer Realtor","Profile", "Terms & Policy", "Privacy Policy"]
    let arrMuneImg: [UIImage] = [#imageLiteral(resourceName: "side-nav1"),#imageLiteral(resourceName: "side-nav2"),#imageLiteral(resourceName: "side-nav3"),#imageLiteral(resourceName: "side-nav10"),#imageLiteral(resourceName: "side-nav5"),#imageLiteral(resourceName: "side-nav6"),#imageLiteral(resourceName: "side-nav7"),#imageLiteral(resourceName: "side-nav7"),#imageLiteral(resourceName: "side-nav8"),#imageLiteral(resourceName: "side-nav10"),#imageLiteral(resourceName: "side-nav11")]
    
    @IBOutlet weak var tblItems: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tblItems.delegate = self
        tblItems.dataSource = self
        tblItems.separatorStyle = .none

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserMenuViewVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tblItems.separatorStyle = .none
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTblCell", for: indexPath) as! MenuTblCell
        cell.lblName.text = arrMenuTitle[indexPath.row]
        cell.img.image = arrMuneImg[indexPath.row]
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 6{
            let activityControler = UIActivityViewController(activityItems: ["www.google.com"], applicationActivities: nil)
                    activityControler.completionWithItemsHandler = {(activity, success, items, error) in
                        if success{
                           // self.lblReferCode?.text = ""
                        }
                    }
                    present(activityControler, animated: true, completion: nil)
                
                
        }else if indexPath.row == 7 {

            let activityControler = UIActivityViewController(activityItems: ["www.google.com"], applicationActivities: nil)
            activityControler.completionWithItemsHandler = {(activity, success, items, error) in
                if success{
                   // self.lblReferCode?.text = ""
                }
            }
            present(activityControler, animated: true, completion: nil)
       
        }
        
        else{

        }

    }
    
    
}
