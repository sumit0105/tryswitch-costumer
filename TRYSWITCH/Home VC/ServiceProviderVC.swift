//
//  ServiceProviderVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 10/10/21.
//

import UIKit

class ServiceProviderVC: UIViewController {
    
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _collectionView: UICollectionView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var firstContView: NSLayoutConstraint!
    @IBOutlet weak var SecContView: NSLayoutConstraint!
    @IBOutlet weak var thirdContView: NSLayoutConstraint!
    @IBOutlet weak var fourContView: NSLayoutConstraint!
    @IBOutlet weak var fiveContView: NSLayoutConstraint!
    @IBOutlet weak var mainContView: NSLayoutConstraint!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var SecondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourView: UIView!
    @IBOutlet weak var fiveView: UIView!
    
    
    var checked = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.separatorStyle = .none
        _collectionView.delegate = self
        _collectionView.dataSource = self
        self.btnCall.borderset()
        self.imgProfile.makeRoundImage()

        
        if firstContView.constant == 400 || SecContView.constant == 400 || thirdContView.constant == 400 || fourContView.constant == 400 || fiveContView.constant == 400 {
            mainContView.constant = 2885
        }else if firstContView.constant == 400 || SecContView.constant == 400 || thirdContView.constant == 400 || fourContView.constant == 400 {
            mainContView.constant = 2485
        }else if firstContView.constant == 400 || SecContView.constant == 400 || thirdContView.constant == 400 {
            mainContView.constant = 2085
        }else if firstContView.constant == 400 || SecContView.constant == 400 {
            mainContView.constant = 1685
        }else if firstContView.constant == 400{
            mainContView.constant = 1685
        }

//         Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.firstView.isHidden = true
        self.SecondView.isHidden = true
        self.thirdView.isHidden = true
        self.fourView.isHidden = true
        self.fiveView.isHidden = true
    }
    
    
    @IBAction func btnService(_ sender: UIButton) {
        
        if checked {
            sender.setImage( UIImage(named:"Garbeg.png"), for: .normal)
            
            self.firstView.isHidden == true
            
            
            self.firstContView.constant = 0
            checked = false
        } else {
            sender.setImage(UIImage(named:"Electricity.png"), for: .normal)
            self.firstView.isHidden == false
            checked = true
        }
        
    }
    
       
    @IBAction func btnACService(_ sender: UIButton) {
        
        if checked {
            sender.setImage( UIImage(named:"Garbeg.png"), for: .normal)
            
            self.SecondView.isHidden == true
            
            //self.SecContView.constant = 0
            checked = false
        } else {
            sender.setImage(UIImage(named:"Electricity.png"), for: .normal)
            self.SecondView.isHidden == false
            self.SecContView.constant = 400
            checked = true
        }
        
    }
    
    
    @IBAction func btnHeater(_ sender: UIButton) {
        
        if checked {
            sender.setImage( UIImage(named:"Garbeg.png"), for: .normal)
            
            self.thirdView.isHidden == true
            
            
            self.thirdContView.constant = 0
            checked = false
        } else {
            sender.setImage(UIImage(named:"Electricity.png"), for: .normal)
            self.thirdView.isHidden == false
            self.thirdContView.constant = 400
            checked = true
        }
        
    }
    
    @IBAction func btnACHeater(_ sender: UIButton) {
        
        if checked {
            sender.setImage( UIImage(named:"Garbeg.png"), for: .normal)
            
            self.fourView.isHidden == true
            
            self.fourContView.constant = 0
            checked = false
        } else {
            sender.setImage(UIImage(named:"Electricity.png"), for: .normal)
            self.fourView.isHidden == true

            self.fourContView.constant = 400
            checked = true
        }
        
    }
    
    
    @IBAction func btnRepair(_ sender: UIButton) {
        
        if checked {
            sender.setImage( UIImage(named:"Garbeg.png"), for: .normal)
            self.fiveView.isHidden == true

            self.fiveContView.constant = 0
            checked = false
        } else {
            sender.setImage(UIImage(named:"Electricity.png"), for: .normal)
            self.fiveView.isHidden == true

            self.fiveContView.constant = 400
            checked = true
        }
        
    }


}

extension ServiceProviderVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ServiceProviderCollCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceProviderCollCell", for: indexPath) as! ServiceProviderCollCell
        return cell
    }
    
}

extension ServiceProviderVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ServiceProviderCell = tableView.dequeueReusableCell(withIdentifier: "ServiceProviderCell") as! ServiceProviderCell
        return cell
    }
    
    
}
