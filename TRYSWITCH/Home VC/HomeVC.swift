//
//  HomeVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 11/10/21.
//

import UIKit
import SideMenuSwift
class HomeVC: SHBaseController {
    
    @IBOutlet weak var tblItems: UITableView!
    @IBOutlet weak var trySwitchPopUp: UIView!
    @IBOutlet weak var listPopup: UIView!
    @IBOutlet weak var sucessPopUp: UIView!
    @IBOutlet weak var bgView: UIView!
    
    var logoarray:[UIImage] = [#imageLiteral(resourceName: "3"),#imageLiteral(resourceName: "1"),#imageLiteral(resourceName: "Untitled1"),#imageLiteral(resourceName: "3"),#imageLiteral(resourceName: "1"),#imageLiteral(resourceName: "Untitled1"),]
    var homearray:[UIImage] = [#imageLiteral(resourceName: "icon-1"),#imageLiteral(resourceName: "icon-2"),#imageLiteral(resourceName: "icon-2"),#imageLiteral(resourceName: "icon-1"),#imageLiteral(resourceName: "icon-2"),#imageLiteral(resourceName: "icon-2")]
    var locstionarray: [UIImage] = [#imageLiteral(resourceName: "location-pointer1"),#imageLiteral(resourceName: "location-pointer1"),#imageLiteral(resourceName: "location-pointer1"),#imageLiteral(resourceName: "location-pointer1"),#imageLiteral(resourceName: "location-pointer1"),#imageLiteral(resourceName: "location-pointer1")]
    var lbl1array = ["107, East trombell center the wood land tx 110072","107, East trombell center the wood land tx 110072","107, East trombell center the wood land tx 110072","107, East trombell center the wood land tx 110072","107, East trombell center the wood land tx 110072","107, East trombell center the wood land tx 110072"]
    var lbl2array = ["lorem ipsom is simple dummy text of the  printong and typcshs indusrty","lorem ipsom is simple dummy text of the  printong and typcshs indusrty","lorem ipsom is simple dummy text of the  printong and typcshs indusrty","lorem ipsom is simple dummy text of the  printong and typcshs indusrty","lorem ipsom is simple dummy text of the  printong and typcshs indusrty","lorem ipsom is simple dummy text of the  printong and typcshs indusrty"]
    var lbl3array = ["398,000","398,000","398,000","398,000","398,000","398,000"]


    @IBOutlet weak var bg2View: UIView!
    @IBOutlet weak var bg1view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblItems.delegate = self
        tblItems.dataSource = self
        NotificationCenter.default.addObserver(forName: NSNotification.Name("profile_click"), object: nil, queue: nil) { (Noti) in
            let status = Noti.object as? Int ?? 101
            print(status)
            if status == 0 {
                
                let vc = FindRealtorsVC1.instantiate(fromAppStoryboard: .Main)
                self.navigationController?.pushViewController(vc, animated: true)
                vc.hidesBottomBarWhenPushed = false
                self.tabBarController?.selectedIndex = 0

            }else if status == 1 {
                
                let vc = _MServProvidVC.instantiate(fromAppStoryboard: .Main)
                self.navigationController?.pushViewController(vc, animated: true)
                vc.hidesBottomBarWhenPushed = false
                self.tabBarController?.selectedIndex = 0
            }else if status == 2 {
                
                let vc = estateJourneyVc.instantiate(fromAppStoryboard: .Service)
                self.navigationController?.pushViewController(vc, animated: true)
                vc.hidesBottomBarWhenPushed = false
                self.tabBarController?.selectedIndex = 0
        }else if status == 3 {
            
            let vc = CheckListVc.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
            vc.hidesBottomBarWhenPushed = false
            self.tabBarController?.selectedIndex = 0
        }else if status == 4 {
            
            let vc = EventVc.instantiate(fromAppStoryboard: .SignUp)
            self.navigationController?.pushViewController(vc, animated: true)
            vc.hidesBottomBarWhenPushed = false
            self.tabBarController?.selectedIndex = 0
        }else if status == 5 {
            
            let vc = sourcesVc.instantiate(fromAppStoryboard: .Service)
            self.navigationController?.pushViewController(vc, animated: true)
            vc.hidesBottomBarWhenPushed = false
            self.tabBarController?.selectedIndex = 0
        }else if status == 6 {
            
            let textToShare = "Hi- I’ve been using TrySwitch which makes it super easy and safe to buy, sell, rent or invest in real estate properties. Sign up now and get access to the most experienced real estate agents and top notch relocation tools."

                if let myWebsite = NSURL(string: "https://www.google.com/") {
                    let objectsToShare = [textToShare, myWebsite] as [Any]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                    //New Excluded Activities Code
                    activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                    //

                    activityVC.popoverPresentationController?.sourceView = self.view
                    self.present(activityVC, animated: true, completion: nil)
                }
            self.tabBarController?.selectedIndex = 0
        }else if status == 7 {
            
            let vc = ContactVc.instantiate(fromAppStoryboard: .Service)
            self.navigationController?.pushViewController(vc, animated: true)
            vc.hidesBottomBarWhenPushed = false
            self.tabBarController?.selectedIndex = 0
        }else if status == 8 {
            
            let vc = ProfileVc.instantiate(fromAppStoryboard: .PopUp)
            self.navigationController?.pushViewController(vc, animated: true)
            vc.hidesBottomBarWhenPushed = false
            self.tabBarController?.selectedIndex = 0
        }else if status == 9 {
            
            let vc = termsAndPriVc.instantiate(fromAppStoryboard: .Service)
            self.navigationController?.pushViewController(vc, animated: true)
            vc.hidesBottomBarWhenPushed = false
            self.tabBarController?.selectedIndex = 0
        }else if status == 10 {
            
            let vc = termsAndPriVc.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
            vc.hidesBottomBarWhenPushed = false
            self.tabBarController?.selectedIndex = 0
        }
        }
        bg1view.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 10)
        bg2View.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 21)
        bgView.topRoundCorner(radius: 20, color: .white, borderWidth: 1)
        trySwitchPopUp.frame = view.bounds
        view.addSubview(trySwitchPopUp)
        trySwitchPopUp.isHidden = true
        listPopup.frame = view.bounds
        view.addSubview(listPopup)
        listPopup.isHidden = true
        sucessPopUp.frame = view.bounds
        view.addSubview(sucessPopUp)
        sucessPopUp.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
//    filterButtion
    
    @IBAction func BtnFltr(_ sender: UIButton) {
  
        filterVc.openDemo(from: self, in: self.view)
        
    }
    
    
    @IBAction func btnTryList(_ sender: UIButton) {
        self.listPopup.isHidden = false
        
    }
    
    @IBAction func sidebarBtn(_ sender: Any) {
//        self.tabBarController?.tabBar.isHidden = true
//        self.hidesBottomBarWhenPushed = true
        sideMenuController?.revealMenu()
    }
    
    @IBAction func sucessPopupBtn(_ sender: UIButton) {
        self.sucessPopUp.isHidden = true
    }
    @IBAction func trySwitchPopupBtn(_ sender: UIButton) {
        self.trySwitchPopUp.isHidden = true
        self.listPopup.isHidden = false
       
    }
    @IBAction func listPopupBtn(_ sender: UIButton) {
        self.listPopup.isHidden = true
        self.sucessPopUp.isHidden = false
    }
       
    @IBAction func floatBtn(_ sender: Any) {
        self.trySwitchPopUp.isHidden = false
    }
    @IBAction func popUpHideBtn1(_ sender: Any) {
        self.sucessPopUp.isHidden = true
    }
    @IBAction func popUpHideBtn2(_ sender: Any) {
        self.listPopup.isHidden = true
    }
    
//    @IBAction func filterBtn(_ sender: Any) {
//        filterVc.openDemo(from: self, in: self.view)
//         
//    }
    
    @IBAction func popUpHideBtn3(_ sender: Any) {
        self.trySwitchPopUp.isHidden = true
    }
    
}


extension HomeVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         homearray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HomeViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeViewCell") as! HomeViewCell
      
        

        cell.imghome.image = homearray[indexPath.row]
        cell.imglocation.image = locstionarray[indexPath.row]
        cell.imglogo.image = logoarray[indexPath.row]
        cell.lblfirst.text = lbl1array[indexPath.row]
        cell.lbltherd.text = lbl2array[indexPath.row]
        cell.lbltherd.text = lbl3array[indexPath.row]
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = H6propertyDetial1.instantiate(fromAppStoryboard: .Main)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
}

