//
//  MenuTblCell.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 04/10/21.
//

import UIKit

class MenuTblCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
