//
//  HomeViewCell.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 14/10/21.
//

import UIKit

class HomeViewCell: UITableViewCell {
    @IBOutlet var imghome: UIImageView!
    @IBOutlet var imglocation: UIImageView!
    @IBOutlet var imglogo: UIImageView!
    @IBOutlet var lblfirst : UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet var lblsecond :UILabel!
    @IBOutlet var lbltherd : UILabel!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5
        )
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
