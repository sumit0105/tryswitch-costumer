//
//  FindRealtorsCell.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 10/10/21.
//

import UIKit
import Cosmos

class FindRealtorsCell: UITableViewCell {

    @IBOutlet weak var _rateView: CosmosView!
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var _nameLbl: UILabel!
    @IBOutlet weak var _msgLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        _rateView.isUserInteractionEnabled = false
        profileImg.makeRoundImage()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
