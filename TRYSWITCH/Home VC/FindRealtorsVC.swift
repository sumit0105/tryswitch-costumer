//
//  HomeVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 11/10/21.
//

import UIKit

class FindRealtorsVC: UIViewController {
    
    @IBOutlet weak var _collectionView: UICollectionView!
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    
    var arrayimg: [UIImage] = [#imageLiteral(resourceName: "facebook-icon"),#imageLiteral(resourceName: "twitter"),#imageLiteral(resourceName: "tiktok"),#imageLiteral(resourceName: "linkdine"),#imageLiteral(resourceName: "youtube")]

    override func viewDidLoad() {
        super.viewDidLoad()
        _collectionView.delegate = self
        _collectionView.dataSource = self
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.separatorStyle = .none
        self.btnMessage.roundCorners(radius: 20)
        self.btnCall.setBorderColor(borderColor: .red, cornerRadiusBound: 20)
        self.imgProfile.makeRoundImage()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  

}
extension FindRealtorsVC: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayimg .count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: FindRealtorsCollCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FindRealtorsCollCell", for: indexPath) as! FindRealtorsCollCell
        cell.socialicon.image = arrayimg[indexPath.row]
        
        return cell
    }
    
    
}
 
extension FindRealtorsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FindRealtorsCell = tableView.dequeueReusableCell(withIdentifier: "FindRealtorsCell") as! FindRealtorsCell
        cell.isUserInteractionEnabled = false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width-10)/7
        return CGSize(width: size, height: size)


        
    
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
}
