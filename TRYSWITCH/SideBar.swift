//
//  SideBar.swift
//  SideMenuPractice
//
//  Created by Anubhav on 26/10/21.
//

import UIKit
import SideMenuSwift


class SideQBar: UIViewController {
    var arrdata = ["Real Estate Agents","Service Providers","My Real Estate Journey","Move-in/Move-out Checklist","My Real Estate Calendar","Real Estate Glossary","Invite Friends","Refer Real Estate Agent","Profile","Terms & Privacy","Privacy Policy"]
    @IBOutlet weak var bgVirw: UIView!
    @IBOutlet var hotspotView1: UIView!
    var arrimg:[String] = ["side-nav1","side-nav2","side-nav3","side-nav4","side-nav5","side-nav6","side-nav7","side-nav8","side-nav9","side-nav10","side-nav11"]
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var sideview: UIView!
    @IBOutlet weak var sidetbar: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        hotspotView1.frame = view.bounds
        self.view.addSubview(hotspotView1)
        hotspotView1.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        bgVirw.roundCornersTopright_TopLeft(radius: 15)
        profileImage.layer.masksToBounds = true
        profileImage.layer.borderWidth = 2.5
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
    }
    override func viewWillAppear(_ animated: Bool) {
        hotspotView1.isHidden = true
    }
    
    @IBAction func EVS(_ sender: Any) {
       
    }
    
    @IBAction func btnDismis(_ sender: Any) {
        
        sideMenuController?.hideMenu()
    }
    @IBAction func dismisBtn(_ sender: Any) {
        self.hotspotView1.isHidden = true
        
        sideMenuController?.hideMenu()
        
    }
    
    
    @IBAction func BtnView1(_ sender: Any) {
        
        sideMenuController?.hideMenu()
        NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 7)
        
    }
    
}
extension SideQBar:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCell =  tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        cell.selectionStyle = .none
        cell.img.image = UIImage(named: arrimg[indexPath.row])
        cell.lbl.text = arrdata[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        switch indexPath.row {
        case 0 :
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 0)
           
        case 1 :
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 1)
           
        case 2 :
            
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 2)
           
        case 3 :
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 3)
          
        case 4 :
            
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 4)
         
        case 5 :
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 5)
           
        case 6 :
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 6)
           
               
        case 7 :
            self.hotspotView1.isHidden = false
          
        case 8 :
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 8)
        
        case 9 :
            sideMenuController?.hideMenu()
            NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 9)
           
        case 10 :
            sideMenuController?.hideMenu()
           // NotificationCenter.default.post(name: NSNotification.Name("profile_click"), object: 10)
            let vc = termsAndPriVc.instantiate(fromAppStoryboard: .Service)
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
   
}


class TableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

@IBDesignable
public class Gradient: UIView {
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}

    override public class var layerClass: AnyClass { CAGradientLayer.self }

    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }

    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updatePoints()
        updateLocations()
        updateColors()
    }
}
